/**
 * 
 */
package com.wwnorton.NLT.BetaAssignment.utilities;

import java.util.Set;

import org.openqa.selenium.WebDriver;

/**
 * @author Ashish D
 *
 * 03-Jan-2020
 */
public class GetWindowHandle {
	
	WebDriver driver;
	
	public void navigatetoChildwindow(){
	String ChildWindow = null;
    Set<String> Windowhandles= driver.getWindowHandles();
    String ParentWindow = driver.getWindowHandle();
    Windowhandles.remove(ParentWindow);
    String winHandle=Windowhandles.iterator().next();
    if (winHandle!=ParentWindow){
    	ChildWindow=winHandle;
    }
    driver.switchTo().window(ChildWindow);
	}
}
