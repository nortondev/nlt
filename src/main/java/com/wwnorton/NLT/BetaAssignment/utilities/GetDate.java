package com.wwnorton.NLT.BetaAssignment.utilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class GetDate {
	
	public static String addDays(int numberOfDays) {
	
	Calendar cal = Calendar.getInstance();
	cal.add(Calendar.DATE, numberOfDays);

	DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");		
	String date =  dateFormat.format(cal.getTime());
	
	return date;
	
	}

}
