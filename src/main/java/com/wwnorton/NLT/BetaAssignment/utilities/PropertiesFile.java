package com.wwnorton.NLT.BetaAssignment.utilities;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.WebDriverWait;




public class PropertiesFile {
	
	public static WebDriver driver = null;
    public static WebDriverWait wait;
    public static String UserDir=System.getProperty("user.dir");
    public static String Browser;
    public static String url;
	public static String pcaturl;
	public static String bookmarkurl;
	public static String DriverPath;
	
	
    public WebDriver getDriver() {
    	
        return driver;
    }
	
    
    // Read Browser name, Test url and Driver file path from config.properties.
    
	public static void readPropertiesFile() throws Exception {
		
		Properties prop = new Properties();
		//System.out.println(UserDir);
		
		try {
			
			InputStream input = new FileInputStream(UserDir + "/src/test/resources/config.properties");
			//InputStream input = new FileInputStream(UserDir +"\\src\\test\\resources\\config.properties");
			prop.load(input);
			
			Browser = prop.getProperty("browsername");
			//url = prop.getProperty("testurl");
			//pcaturl = prop.getProperty("pcattesturl");
			//bookmarkurl = prop.getProperty("bookmarktesturl");
			
			NLTConfig nltConfig = UpdateConfigFile.updateConfigurations();
			
			url = nltConfig.getTesturl();
			pcaturl = nltConfig.getPcattesturl();
			bookmarkurl = nltConfig.getBookmarktesturl();
			
			//DriverPath = UserDir + "\\Drivers\\";
			DriverPath = UserDir + "/Drivers/";
		
		} catch (Exception e) {
			
			e.printStackTrace();
		}

		
	}	

	
	// Set Browser configurations by comparing Browser name and Diver file path.
	
	public static void setBrowserConfig() {
		
			if(Browser.contains("Firefox")) {
				System.setProperty("webdriver.gecko.driver", DriverPath + "geckodriver.exe");
            
				FirefoxOptions firefoxOptions = new FirefoxOptions();
				firefoxOptions.setHeadless(false);
				firefoxOptions.setCapability("marionette", true);
            
				driver = new FirefoxDriver(firefoxOptions); 
            
			}
			
			if(Browser.contains("Chrome")) {
				System.setProperty("webdriver.chrome.driver", DriverPath + "chromedriver.exe");
				
				//System.setProperty("webdriver.chrome.driver", "C:\\Users\\wwnortonqa\\Downloads\\chromedriver.exe");
				
				//DesiredCapabilities desiredCapabilities = DesiredCapabilities.chrome();
				
				ChromeOptions chromeOptions = new ChromeOptions();
				//chromeOptions.setHeadless(false);
				//chromeOptions.addArguments("test-type");
				//chromeOptions.setPageLoadStrategy(PageLoadStrategy.EAGER);
				//chromeOptions.setBinary("/Applications/Google Chrome.app/Contents/MacOS/Google Chrome");
				chromeOptions.addArguments("start-maximized"); 
				chromeOptions.addArguments("enable-automation");
				chromeOptions.addArguments("--disable-infobars");
				chromeOptions.addArguments("--disable-extensions");
				chromeOptions.addArguments("--dns-prefetch-disable");
				chromeOptions.addArguments("--disable-gpu");
				chromeOptions.addArguments("--no-default-browser-check");
				chromeOptions.addArguments("--ignore-certificate-errors");
				chromeOptions.addArguments("--proxy-server='direct://'");
				chromeOptions.addArguments("--proxy-bypass-list=*");
				chromeOptions.setExperimentalOption("useAutomationExtension", false);
				//desiredCapabilities.setCapability(ChromeOptions.CAPABILITY, chromeOptions);
				driver = new ChromeDriver(chromeOptions);
				

           }
			
		}
		

	// Set Test URL based on config.properties file.
	
	
	public static void setURL(String productURL) {
		
		driver.manage().window().maximize();
		driver.get(url + productURL);
	
	}
	
	
	public static void setTheysay4URL(WebDriver newdriver, String productURL) {
		
		newdriver.manage().window().maximize();
		newdriver.get(url + productURL);
	
	}
	
	
	public static void setSW5URL(WebDriver newdriver, String productURL) {
		
		newdriver.manage().window().maximize();
		newdriver.get(url + productURL);
	
	}
	
	public static void setBookMarkURL() {
		
		driver.manage().window().maximize();
		driver.get(bookmarkurl);
	
	}
	
	public static void setPCATURL() {
		
		driver.manage().window().maximize();
		driver.get(pcaturl);
	
	}
		
	
	// Close the driver after running test script.
	
	public static void tearDownTest() throws InterruptedException {
		
			wait = new WebDriverWait(driver,5);
			driver.close();
			driver.quit();
					
		}


}
