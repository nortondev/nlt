package com.wwnorton.NLT.BetaAssignment.utilities;

import java.io.IOException;

public class UpdateConfigFile extends ReadApplicationProperties {

	// Call to Application Properties file to read environment value.
	public static NLTConfig updateConfigurations()
			throws org.apache.commons.configuration.ConfigurationException, IOException {

		NLTConfig nltConfig = new NLTConfig();

		String varEnv = ReadApplicationProperties.getEenvironmentValue();
		// PropertiesConfiguration config = new PropertiesConfiguration(UserDir +
		// "/src/test/resources/config.properties");

		switch (varEnv) {

		case "QA":
			varEnv.equalsIgnoreCase("QA");
			nltConfig.setTesturl("https://ncia-nlt-qa.wwnorton.net/");
			nltConfig.setBookmarktesturl("https://nlt-qa.wwnorton.net/course/1477");
			nltConfig.setPcattesturl("https://ncia-nlt-qa.wwnorton.net/activity_editor/index.php?");

			// config.setProperty("testurl", "https://ncia-nlt-qa.wwnorton.net/");
			// config.setProperty("bookmarktesturl",
			// "https://nlt-qa.wwnorton.net/course/1477");
			// config.setProperty("pcattesturl", 
			//"https://ncia-nlt-qa.wwnorton.net/activity_editor/index.php?");
			// config.save();

			break;

		case "Prod":
			varEnv.equalsIgnoreCase("Prod");
			varEnv.equalsIgnoreCase("QA");
			nltConfig.setTesturl("https://digital.wwnorton.com/");
			nltConfig.setBookmarktesturl("https://nlt.wwnorton.com/course/216679");
			nltConfig.setPcattesturl("https://ncia-authoring.wwnorton.com/activity_editor/index.php?");
			break;

		case "Stg":
			varEnv.equalsIgnoreCase("Stg");
			varEnv.equalsIgnoreCase("QA");
			nltConfig.setTesturl("https://ncia-nlt-qa.wwnorton.net/");
			nltConfig.setBookmarktesturl("https://nlt-qa.wwnorton.net/course/1477");
			nltConfig.setPcattesturl("https://ncia-nlt-qa.wwnorton.net/activity_editor/index.php?");
			break;

		case "UAT":
			varEnv.equalsIgnoreCase("UAT");
			varEnv.equalsIgnoreCase("QA");
			nltConfig.setTesturl("https://ncia-nlt-qa.wwnorton.net/");
			nltConfig.setBookmarktesturl("https://nlt-qa.wwnorton.net/course/1477");
			nltConfig.setPcattesturl("https://ncia-nlt-qa.wwnorton.net/activity_editor/index.php?");
			break;

		}

		return nltConfig;

	}

}

class NLTConfig {

	private String testurl;
	private String bookmarktesturl;
	private String pcattesturl;

	public String getTesturl() {
		return testurl;
	}

	public void setTesturl(String testurl) {
		this.testurl = testurl;
	}

	public String getBookmarktesturl() {
		return bookmarktesturl;
	}

	public void setBookmarktesturl(String bookmarktesturl) {
		this.bookmarktesturl = bookmarktesturl;
	}

	public String getPcattesturl() {
		return pcattesturl;
	}

	public void setPcattesturl(String pcattesturl) {
		this.pcattesturl = pcattesturl;
	}

}
