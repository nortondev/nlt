package com.wwnorton.NLT.BetaAssignment.objectFactory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import ru.yandex.qatools.allure.annotations.Step;



public class eBookDetails extends SelectProductDLP {
	
WebDriver driver;
	
	
	//Finding WebElements on NLT Home page using PageFactory
	
	@FindBy(how = How.XPATH, using = "//div[@id='book-title']")
	public WebElement courseTitle;
	
	@FindBy(how = How.XPATH, using = "//div[@class='edition']")
	public WebElement courseEdition;
	
	@FindBy(how = How.XPATH, using = "//div[@class='author']")
	public WebElement courseAuthor;

	@FindBy(how = How.XPATH, using = "//button[@class='more-info secondary']")
	public WebElement moreInfoButton;
	
	@FindBy(how = How.XPATH, using = "//a[@class='open-ebook']")
	public WebElement openeBookButton;
	
	@FindBy(how = How.XPATH, using = "//div[@class='ebook-info-title']")
	public WebElement eBookTitle;
	
	@FindBy(how = How.XPATH, using = "//div[@class='ebook-info-subtitle']")
	public WebElement eBookSubTitle;
	
	@FindBy(how = How.XPATH, using = "//div[@class='ebook-info-edition']")
	public WebElement eBookEdition;
	
	@FindBy(how = How.XPATH, using = "//div[@class='ebook-info-author']/span[1]")
	public WebElement eBookAuthor;
	
	@FindBy(how = How.XPATH, using = "//div[@class='ebook-info-readingline']")
	public WebElement eBookReadingLine;
	
	@FindBy(how = How.XPATH, using = "//div[@class='ebook-info-lowerdiv']")
	public WebElement eBookBottomDiv;
	
	@FindBy(how = How.XPATH, using = "//a[@class='ebook-info-open']")
	public WebElement openeBookButton_BookDetails_Overlay;
	
	@FindBy(how = How.XPATH, using = "//a[contains(text(),'Chapter 1: The Science of Psychology')]")
	public WebElement firstBookTitle;
	
	@FindBy(how = How.XPATH, using = "//div[@id='return_to_resources_button']//img[@class='return_arrow']")
	public static WebElement returnToDLP;
	
	@FindBy(how = How.XPATH, using = "/html[1]/body[1]/div[2]/div[1]/div[1]/div[1]/button[1]")
	public WebElement closeeBookDetailsOverlayModal;
	
	
	
	// Initializing Web Driver and PageFactory.
	
	public eBookDetails(WebDriver driver) throws Exception {
		
		super(driver);
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	
	
	@Step("Click on More Info button to see Book Details Overlay, Method: {method}")
	public void clickMoreInfoButton() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		wait.until(ExpectedConditions.elementToBeClickable(moreInfoButton));
		moreInfoButton.click();
	
	}
		
		
	@Step("Verify that eBook is opened in a new tab, Method: {method}")
	public void clickOpeneBookButton() throws Exception {
			
		WebDriverWait wait = new WebDriverWait(driver,3);
		wait.until(ExpectedConditions.elementToBeClickable(openeBookButton_BookDetails_Overlay));
		openeBookButton_BookDetails_Overlay.click();
		
	}
	
	
	@Step("Capture eBook URL from DLP page, Method: {method}")
	public String captureeBookURL_DLP(String productName) throws Exception {
				
		WebDriverWait wait = new WebDriverWait(driver,3);
		wait.until(ExpectedConditions.elementToBeClickable(firstBookTitle));
		firstBookTitle.click();
		
		
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
	
		Thread.sleep(3000);
		String eBookurl = driver.getCurrentUrl();
		
		// Close the new window, if that window no more required
		driver.close();
		
		return eBookurl;
		
	}
	

}
