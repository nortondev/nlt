package com.wwnorton.NLT.BetaAssignment.objectFactory;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import ru.yandex.qatools.allure.annotations.Step;

public class ProductConfig_PCAT {
	
WebDriver driver;
	
	
	//Finding WebElements on NLT Home page using PageFactory
	
	@FindBy(how = How.XPATH, using = "//div[@id='selectors']//select[1]")
	public WebElement disciplineDropDown;
	
	@FindBy(how = How.XPATH, using = "//div[@id='selectors']//select[2]")
	public WebElement productDropDown;
	
	@FindBy(how = How.XPATH, using = "//div[@id='pe_product_info_holder']")
	public WebElement productInfoSection;
	
	// @FindBy(how = How.XPATH, using = "//div[@id='pe_product_info_holder']//img")
	// public WebElement productImage;
	
	 @FindBy(how = How.XPATH, using = "//div[@id='pe_product_info_holder']//div[1]")
	 public WebElement productTitle;
	
	 //@FindBy(how = How.XPATH, using = "//div[@id='pe_product_info_holder']//div[2]")
	// public WebElement productSubTitle;
	
	 @FindBy(how = How.XPATH, using = "//div[@id='pe_product_info_holder']//div[2]")
	 public WebElement productEdition;
	
	 @FindBy(how = How.XPATH, using = "//div[@id='pe_product_info_holder']//div[3]")
	 public WebElement productAuthor;
	
	// @FindBy(how = How.XPATH, using = "//div[@id='pe_product_info_holder']//div[5]")
	 //public WebElement productReadingLine;
	
	 @FindBy(how = How.XPATH, using = "//div[@id='pe_product_info_holder']//div[5]")
	 public WebElement productBottomDiv;
	
	@FindBy(how = How.XPATH, using = "//html[1]/body[1]/div[7]/div[2]/button[1]/span[1]")
	public WebElement editProductInfoButton;
	
	@FindBy(how = How.XPATH, using = "//input[@id='pe_edit_title_info_title']")
	public WebElement editProductTitle;
	
	@FindBy(how = How.XPATH, using = "//input[@id='pe_edit_title_info_subtitle']")
	public WebElement editProductSubTitle;
	
	@FindBy(how = How.XPATH, using = "//input[@id='pe_edit_title_info_edition']")
	public WebElement editProductEdition;
	
	@FindBy(how = How.XPATH, using = "//input[@id='pe_edit_title_info_authors']")
	public WebElement editProductAuthor;
	
	@FindBy(how = How.XPATH, using = "//input[@id='pe_edit_title_info_readingline']")
	public WebElement editProductReadingLine;
	
	@FindBy(how = How.XPATH, using = "//div[@id='pe_edit_title_info_content']")
	public WebElement editProductDetails;
	
	@FindBy(how = How.XPATH, using = "//input[@id='pe_edit_title_info_dlp_bottom_div']")
	public WebElement editDLPBottomDiv;
	
	@FindBy(how = How.XPATH, using = "//div[@class='ui-dialog-buttonpane ui-widget-content ui-helper-clearfix']//button[1]")
	public WebElement saveProductInfoButton;
	
	@FindBy(how = How.XPATH, using = "//span[@class='ui-button-text']//img")
	public WebElement gearMenuIcon;
	
	@FindBy(how = How.XPATH, using = "//b[contains(text(),'Sign Out')]")
	public WebElement signOutLink;
	
	
	
	// Initializing Web Driver and PageFactory.
	
	public ProductConfig_PCAT(WebDriver driver) throws Exception {
		
		this.driver = driver;
		PageFactory.initElements(driver, this);
		
	}
	
	
	@Step("Select a Discipline, Method: {method}")
	public void selectDiscipline(String DisciplineName) throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		wait.until(ExpectedConditions.visibilityOf(disciplineDropDown));
		Select drpDiscipline = new Select(disciplineDropDown);
		drpDiscipline.selectByValue(DisciplineName);
	
	}
	
	
	@Step("Select a Product, Method: {method}")
	public void selectProduct(String ProductName) throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		wait.until(ExpectedConditions.visibilityOf(productDropDown));
		Select drpProduct = new Select(productDropDown);
		drpProduct.selectByValue(ProductName);
	
	}
	
	
	@Step("Remove Product Title,  Method: {method} ")
	public void removeProductTitle() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(editProductTitle));
		String zipLen = editProductTitle.getAttribute("value");
		int lenText = zipLen.length();

		for(int i = 0; i < lenText; i++){
			editProductTitle.sendKeys(Keys.ARROW_LEFT);
			editProductTitle.sendKeys(Keys.DELETE);
			
			}
		}
	
	
	@Step("Remove Product SubTitle,  Method: {method} ")
	public void removeProductSubTitle() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(editProductSubTitle));
		String zipLen = editProductSubTitle.getAttribute("value");
		int lenText = zipLen.length();

		for(int i = 0; i < lenText; i++){
			editProductSubTitle.sendKeys(Keys.ARROW_LEFT);
			editProductSubTitle.sendKeys(Keys.DELETE);
			
			}
		}
	
	@Step("Remove Product Edition,  Method: {method} ")
	public void removeProductEdition() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(editProductEdition));
		String zipLen = editProductEdition.getAttribute("value");
		int lenText = zipLen.length();

		for(int i = 0; i < lenText; i++){
			editProductEdition.sendKeys(Keys.ARROW_LEFT);
			editProductEdition.sendKeys(Keys.DELETE);
			
			}
		}
	
	@Step("Remove Product Author,  Method: {method} ")
	public void removeProductAuthor() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(editProductAuthor));
		String zipLen = editProductAuthor.getAttribute("value");
		int lenText = zipLen.length();

		for(int i = 0; i < lenText; i++){
			editProductAuthor.sendKeys(Keys.ARROW_LEFT);
			editProductAuthor.sendKeys(Keys.DELETE);
			
			}
		}
	
	
	@Step("Remove Product Reading Line,  Method: {method} ")
	public void removeProductReadingLine() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(editProductReadingLine));
		String zipLen = editProductReadingLine.getAttribute("value");
		int lenText = zipLen.length();

		for(int i = 0; i < lenText; i++){
			editProductReadingLine.sendKeys(Keys.ARROW_LEFT);
			editProductReadingLine.sendKeys(Keys.DELETE);
			
			}
		}
	
	
	@Step("Remove Product Details,  Method: {method} ")
	public void removeProductDetails() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		String zipLen;
		
		try 
		{
			wait.until(ExpectedConditions.visibilityOf(editProductDetails));
			zipLen = editProductDetails.getAttribute("value");
			
			if (zipLen != null) {
				int lenText = zipLen.length();
				for(int i = 0; i < lenText; i++){
					editProductDetails.sendKeys(Keys.ARROW_LEFT);
					editProductDetails.sendKeys(Keys.DELETE);
			
				}
			} 
		} 
		
		catch (NullPointerException e) 
		{

			System.out.println("Exception Handlled: " + e.getMessage());
			
		}
	}

	
	@Step("Remove Product DLP Bottom Division,  Method: {method} ")
	public void removeProductDLPBottomDiv() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		TimeUnit.SECONDS.sleep(3);
		
		wait.until(ExpectedConditions.visibilityOf(editDLPBottomDiv));
		String zipLen = editDLPBottomDiv.getAttribute("value");
		int lenText = zipLen.length();

		for(int i = 0; i < lenText; i++){
			editDLPBottomDiv.sendKeys(Keys.ARROW_LEFT);
			editDLPBottomDiv.sendKeys(Keys.DELETE);
			
			}
		}
	
	
	@Step("Get Product Information, Method: {method}")
	public void captureProductInfo() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		wait.until(ExpectedConditions.visibilityOf(productInfoSection));
	
	}
	
	
	@Step("Edit Product Information, Method: {method}")
	public void clickEditProductInfoButton() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		wait.until(ExpectedConditions.visibilityOf(editProductInfoButton));
		
		editProductInfoButton.click();
	
	}
	
	
	@Step("Sign Out of PCAT application, Method: {method}")
	public void signOutPCAT() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(driver,3);
		wait.until(ExpectedConditions.elementToBeClickable(gearMenuIcon));
		
		gearMenuIcon.click();
		
		wait.until(ExpectedConditions.elementToBeClickable(signOutLink));
		signOutLink.click();
		
	}

}
