
/* Verify that student should not able to see the course selection pop up 
 * when he/she is added in to two courses, end date for one course has been passed 
 * and other course in within GAU */

package com.wwnorton.NLT.BetaAssignment;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.wwnorton.NLT.BetaAssignment.objectFactory.BetaAssignmentPage;
import com.wwnorton.NLT.BetaAssignment.objectFactory.LaunchBetaAssignment;
import com.wwnorton.NLT.BetaAssignment.objectFactory.LoginPage;
import com.wwnorton.NLT.BetaAssignment.objectFactory.SelectProductDLP;
import com.wwnorton.NLT.BetaAssignment.objectFactory.SetAssignmentGAU;
import com.wwnorton.NLT.BetaAssignment.utilities.GetDate;
import com.wwnorton.NLT.BetaAssignment.utilities.PropertiesFile;
import com.wwnorton.NLT.BetaAssignment.utilities.ReadJsonFile;
import com.wwnorton.NLT.BetaAssignment.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;



//Call to TestNG listeners to save test logs and attachments as screen shots on Test failure.

@Listeners({ TestListener.class })


public class VerifyCourseSelectionPopUpIsNOTDisplayed_OneActive_OneExpired extends PropertiesFile {
	
	LoginPage NLTLogin;
	LaunchBetaAssignment GoToBetaAssignment;
	SetAssignmentGAU GAUAssignment;
	BetaAssignmentPage ViewBetaAssignment;
	SelectProductDLP ProductDLP;
	
	String userName;
	String Password;
	String LoginName;
	
	String strGAUDate;
	String jsonProductName;
	String jsonSSName1;
	String jsonSSName2;
	String jsonCourseName;
	String BetaViewCourseName;
	
	
	// TestNG Annotations
			@Parameters("Browser")
			@BeforeTest
			// Call to Properties file initiate Browser and set Test URL.

			public void callPropertiesFile() throws Exception {

				PropertiesFile.readPropertiesFile();
				PropertiesFile.setBrowserConfig();
				PropertiesFile.setURL("psychsci5");

			}

			// Read login data from Json File
			ReadJsonFile readJasonObject = new ReadJsonFile();
			JsonObject jsonobject = readJasonObject.readJson();
			

			// Allure Annotations

			@Severity(SeverityLevel.NORMAL)
			@Description("Verify that student should not able to see the Course Selection pop up \n" + 
					" when he/she is added in to two courses, one is with end date and other \n" +
					"is within GAU")
			@Stories("Course Information")
			
			
			@Test(priority = 0)
			public void TC15_VerifyCourseSelectionPopUpIsNOTDisplayed_OneActive_OneExpired() throws Exception { 
				
				//Login to NCIA application as an Instructor.
				
				NLTLogin = new LoginPage(driver);
				
				userName = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("userName").getAsString();
				Password = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("password").getAsString();
				
				NLTLogin.loginNLT(userName, Password);
				
				wait = new WebDriverWait(driver,5);
				wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
				wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));
				
				Thread.sleep(10000);

				LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
				NLTLogin.verifySignin(LoginName, userName);
					
			
				// Set Assignment GAU Date for StudentSet01 - Assignment with Due Date.  
				
				ProductDLP = new SelectProductDLP(driver);
				
				jsonProductName = jsonobject.getAsJsonObject("ProductDLP").get("productName1").getAsString();
				
				Thread.sleep(3000);
				SelectProductDLP.clickProductImage(jsonProductName);
				SelectProductDLP.clickOKButton();
				
				GAUAssignment = new SetAssignmentGAU(driver);
				
				jsonSSName1 = jsonProductName = jsonobject.getAsJsonObject("StudentSet").get("studentSetName7").getAsString();
				ProductDLP.selectSSByTitle(jsonSSName1);
				
				Thread.sleep(3000);
				GAUAssignment.removeAllGAUDates();
				
				String futureDatePlusThirty = GetDate.addDays(30);
				
				Thread.sleep(3000);
				GAUAssignment.setGAUDate(futureDatePlusThirty);

			
				
				// Set Assignment GAU Date for StudentSet02 - Assignment with Past Date. 
							
				GAUAssignment = new SetAssignmentGAU(driver);
							
				Thread.sleep(3000);
				jsonSSName2 = jsonProductName = jsonobject.getAsJsonObject("StudentSet").get("studentSetName8").getAsString();
				ProductDLP.selectSSByTitle(jsonSSName2);
							
				Thread.sleep(3000);
				GAUAssignment.removeAllGAUDates();
				
				String pastDateMinusThirty = GetDate.addDays(-30);
							
				Thread.sleep(3000);
				GAUAssignment.setGAUDate(pastDateMinusThirty);

			

				//Logout of NCIA application as an Instructor.
				
				NLTLogin.logoutNCIA();
			
			
				//Login to NLT Application as Student and verify Student is logged in successfully.
				
				NLTLogin = new LoginPage(driver);
				
				userName = jsonobject.getAsJsonObject("StudentLoginCredentials").get("student2").getAsString();
				Password = jsonobject.getAsJsonObject("StudentLoginCredentials").get("password2").getAsString();
				
				Thread.sleep(3000);
				NLTLogin.loginNLT(userName, Password);
				
				wait = new WebDriverWait(driver,5);
				wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
				wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));
				
				Thread.sleep(10000);
				LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
				NLTLogin.verifySignin(LoginName, userName);
					
			
				//Verify that Course Selection Pop-Up is Not displayed after launching Beta Assignment View.
				
				GoToBetaAssignment = new LaunchBetaAssignment(driver);
	
				Thread.sleep(3000);
				GoToBetaAssignment.ClickBetaAssignmentButton();
				
				WebDriverWait wait = new WebDriverWait(driver, 10);
				TimeUnit.SECONDS.sleep(5);
				
				try {
						wait.until(ExpectedConditions.visibilityOf(GoToBetaAssignment.SelectCourseModal));
						boolean SelectCourseModalExist = GoToBetaAssignment.SelectCourseModal.isDisplayed();
				
							if(SelectCourseModalExist == true) {
					
								Assert.assertEquals(SelectCourseModalExist, false, "Course Information Pop-up is displayed");
				
							}	
					}
			
					catch (Exception e) {
					
							System.out.println("Course Information Pop-up is NOT displayed");
						}
			
			
				// Validate the selected Course Name is displayed in Beta View Assignment page.
				
				ViewBetaAssignment = new BetaAssignmentPage(driver);
				
				jsonCourseName = jsonobject.getAsJsonObject("CourseInformation").get("courseName7").getAsString();
				
				boolean CourseNameExist;
				
				try {	
						CourseNameExist = ViewBetaAssignment.SelectedCourseName.isDisplayed(); 
						
						if (CourseNameExist == true) {
							
							BetaViewCourseName = ViewBetaAssignment.SelectedCourseName.getText();
							ViewBetaAssignment.checkSelectedBetaAssignment(jsonCourseName, BetaViewCourseName);
							
						}
						
				}
				
				catch (Exception Ex) {
					
					System.out.println("Course Information is NOT displayed in Beta Assignment View");
					
				}
			
			
				// Logout of NLT application as Student.
				
				NLTLogin.logoutNLT();
			}
			

			
			// Closing driver and test.

			@AfterTest
			public void closeTest() throws Exception {

				PropertiesFile.tearDownTest();

			}

}



