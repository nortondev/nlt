package com.wwnorton.NLT.BetaAssignment;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.wwnorton.NLT.BetaAssignment.utilities.UpdateConfigFile;
import ru.yandex.qatools.allure.annotations.Step;

import com.wwnorton.NLT.BetaAssignment.utilities.TestListener;


public class SetUpEnvAndTestData extends UpdateConfigFile {
	
	TestListener testHelper = new TestListener();

	@BeforeSuite
	//@org.testng.annotations.Test
	public void setUpEEnvironment() throws Exception {
		
		// Call to Config file to get the updated test url's.
		
		//UpdateConfigFile.updateConfigurations();
		
		
	}
	
	@Step("Update StudentSet data and Course ID,  Method: {method}")
	public void setUpTestData() throws Exception {
		
		// Update StudentSet data based on Environment value.
		// Update Invalid Course ID based on Environment value.
		
	}
	
	@AfterSuite
	public void cleanupAfterTestSuite() throws Exception {
		// Do any cleanup activity
	}

}
