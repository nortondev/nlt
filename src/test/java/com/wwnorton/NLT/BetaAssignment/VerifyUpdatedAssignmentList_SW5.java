
/* SW5 - Verify that student should be able to see the updated assignment list 
when he/she opens up the BETA ASSIGNMENT VIEW and simultaneously instructor assigns 
and unassigns the assignment.*/

package com.wwnorton.NLT.BetaAssignment;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.wwnorton.NLT.BetaAssignment.objectFactory.BetaAssignmentPage;
import com.wwnorton.NLT.BetaAssignment.objectFactory.LaunchBetaAssignment;
import com.wwnorton.NLT.BetaAssignment.objectFactory.LoginPage;
import com.wwnorton.NLT.BetaAssignment.objectFactory.SelectProductDLP;
import com.wwnorton.NLT.BetaAssignment.objectFactory.SetAssignmentGAU;
import com.wwnorton.NLT.BetaAssignment.utilities.GetDate;
import com.wwnorton.NLT.BetaAssignment.utilities.PropertiesFile;
import com.wwnorton.NLT.BetaAssignment.utilities.ReadJsonFile;
import com.wwnorton.NLT.BetaAssignment.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;



//Call to TestNG listeners to save test logs and attachments as screen shots on Test failure.

@Listeners({ TestListener.class })


public class VerifyUpdatedAssignmentList_SW5 extends PropertiesFile {
	
	LoginPage NLTLogin;
	LaunchBetaAssignment GoToBetaAssignment;
	BetaAssignmentPage ViewBetaAssignment;
	SetAssignmentGAU GAUAssignment;
	SelectProductDLP ProductDLP;
	
	String userName;
	String Password;
	String LoginName;
	String strCourseName;
	String BetaViewVourseName;
	String jCourseName;
	
	String strGAUDate;
	String jsonProductName;
	String jsonSSName;
	
	String title1;
	String title2;
	String title3;
	
	WebDriver driver2;
	String studentWindowHandle;
	String instructorWindowHandle;
	
	
	// TestNG Annotations
			@Parameters("Browser")
			@BeforeTest
			// Call to Properties file initiate Browser and set Test URL.

			public void callPropertiesFile() throws Exception {

				PropertiesFile.readPropertiesFile();
				PropertiesFile.setBrowserConfig();
				PropertiesFile.setURL("chem5");
				//PropertiesFile.setSW5URL();

			}

			// Read login data from Json File
			ReadJsonFile readJasonObject = new ReadJsonFile();
			JsonObject jsonobject = readJasonObject.readJson();
			

			// Allure Annotations

			@Severity(SeverityLevel.NORMAL)
			@Description("Verify that student should be able to see the updated assignment list")
			@Stories("Assignment List")
			
			
			@Test(priority = 0)
			public void TC_19VerifyUpdatedAssignmentList_SW5() throws Exception { 
				
				//Login to NCIA application as an Instructor in Chrome Browser.
				
				NLTLogin = new LoginPage(driver);
				
				userName = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("userName").getAsString();
				Password = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("password").getAsString();
				
				NLTLogin.loginNLT(userName, Password);
				
				wait = new WebDriverWait(driver,5);
				wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
				wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));
				
				Thread.sleep(10000);

				LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
				NLTLogin.verifySignin(LoginName, userName);

			
				// Set Assignment GAU Date for 1st Assignment
				
				ProductDLP = new SelectProductDLP(driver);
				
				
				jsonProductName = jsonobject.getAsJsonObject("ProductDLP").get("productName2").getAsString();
				
				Thread.sleep(3000);
				SelectProductDLP.clickProductImage(jsonProductName);
				SelectProductDLP.clickOKButton();
				
				jsonSSName = jsonProductName = jsonobject.getAsJsonObject("StudentSet").get("studentSetName2").getAsString();
				ProductDLP.selectSSByTitle(jsonSSName);
				
				GAUAssignment = new SetAssignmentGAU(driver);
				
				// Store the current window handle
				String winHandleBefore = driver.getWindowHandle();
				
				// Perform the click operation that opens new window
				Thread.sleep(3000);
				GAUAssignment.editAssignment_SetGAU();
				
				for (String winHandle : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);
				}
				
				Thread.sleep(25000);
				driver.switchTo().frame("swfb_iframe");
				
				title1 = GAUAssignment.getAssignmentName();
				
				String futureDatePlusThirty = GetDate.addDays(30);
				
				Thread.sleep(3000);
				GAUAssignment.setSW5GAUDate(futureDatePlusThirty);
				
				Thread.sleep(2000);
				GAUAssignment.publishButton();
				
				// Close the new window, if that window no more required
				driver.close();
				
				// Switch back to original browser (first window)
				driver.switchTo().window(winHandleBefore);
				
				
				//Logout of NCIA application as an Instructor in Chrome Browser.
				
				NLTLogin.logoutNCIA();
			
			
				//Login to NLT Application as Student and verify Student is logged in successfully.
				
				NLTLogin = new LoginPage(driver);
				
				userName = jsonobject.getAsJsonObject("StudentLoginCredentials").get("student1").getAsString();
				Password = jsonobject.getAsJsonObject("StudentLoginCredentials").get("password1").getAsString();
				
				Thread.sleep(3000);
				NLTLogin.loginNLT(userName, Password);
				
				wait = new WebDriverWait(driver,10);
				wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
				wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));
				
				Thread.sleep(10000);
				LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
				NLTLogin.verifySignin(LoginName, userName);

			
				//Launch Beta Assignment View after selecting Course.
				
				GoToBetaAssignment = new LaunchBetaAssignment(driver);
	
				Thread.sleep(3000);
				GoToBetaAssignment.ClickBetaAssignmentButton();
				
				strCourseName = jsonobject.getAsJsonObject("CourseInformation").get("courseName2").getAsString();
				Thread.sleep(3000);
				GoToBetaAssignment.SelectNLTCourse(strCourseName);
			
			
				//Compare 1st Assignment title published in DLP Page.
			
				ViewBetaAssignment = new BetaAssignmentPage(driver);
				
				Thread.sleep(5000);
				ViewBetaAssignment.validateAssignmentNameExist(title1);
				
				studentWindowHandle = driver.getWindowHandle();

			
				//Open different browser and login as Instructor.
				
				PropertiesFile.readPropertiesFile();

				System.setProperty("webdriver.gecko.driver", DriverPath + "geckodriver.exe");
				FirefoxOptions firefoxOptions = new FirefoxOptions();
				firefoxOptions.setHeadless(false);
				firefoxOptions.setCapability("marionette", true);
				driver2 = new FirefoxDriver(firefoxOptions);
				//driver2.manage().window().maximize();
				PropertiesFile.setSW5URL(driver2, "chem5");
				//driver2.get("https://ncia-nlt-qa.wwnorton.net/chem5");
				
				NLTLogin = new LoginPage(driver2);
				
				userName = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("userName").getAsString();
				Password = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("password").getAsString();
				
				NLTLogin.loginNLT(userName, Password);
				
				wait = new WebDriverWait(driver,5);
				wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
				wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));
				
				Thread.sleep(10000);
				LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
				NLTLogin.verifySignin(LoginName, userName);

			
				// Set Assignment GAU Date for 2nd Assignment
				
				ProductDLP = new SelectProductDLP(driver2);
				
				
				jsonProductName = jsonobject.getAsJsonObject("ProductDLP").get("productName2").getAsString();
				
				Thread.sleep(3000);
				SelectProductDLP.clickProductImage(jsonProductName);
				SelectProductDLP.clickOKButton();
				
				jsonSSName = jsonProductName = jsonobject.getAsJsonObject("StudentSet").get("studentSetName2").getAsString();
				ProductDLP.selectSSByTitle(jsonSSName);
				
				GAUAssignment = new SetAssignmentGAU(driver2);
				
				// Store the current window handle
				String winHandleBefore1 = driver2.getWindowHandle();
				
				// Perform the click operation that opens new window
				Thread.sleep(3000);
				GAUAssignment.editAssignment_SetGAU();
				
				for (String winHandle1 : driver2.getWindowHandles()) {
					driver2.switchTo().window(winHandle1);
				}
				
				Thread.sleep(25000);
				driver2.switchTo().frame("swfb_iframe");
				
				title2 = GAUAssignment.getAssignmentName();
				
				String futureDatePlusTwenty = GetDate.addDays(20);
				
				Thread.sleep(3000);
				GAUAssignment.setSW5GAUDate(futureDatePlusTwenty);
				
				Thread.sleep(2000);
				GAUAssignment.publishButton();
				
				// Close the new window, if that window no more required
				driver2.close();
				
				// Switch back to original browser (first window)
				driver2.switchTo().window(winHandleBefore1);
				
				instructorWindowHandle = driver2.getWindowHandle();

			
				//Switch to Student Beta Assignment view and Compare 2nd Assignment title published in DLP Page.
				
				ViewBetaAssignment = new BetaAssignmentPage(driver);
				
				Thread.sleep(3000);
				driver.switchTo().window(studentWindowHandle);
				
				Thread.sleep(3000);
				ViewBetaAssignment.validateAssignmentNameExist(title2);

			
				//Switch to Instructor window and remove GAU date for 1st Assignment in DLP Page.
				
				Thread.sleep(3000);
				GAUAssignment = new SetAssignmentGAU(driver2);
				
				driver2.switchTo().window(instructorWindowHandle);
				
				String winHandleBefore2 = driver2.getWindowHandle();
				
				// Perform the click operation that opens new window
				Thread.sleep(3000);
				GAUAssignment.editAssignment_ClearGAU();
				
				for (String winHandle2 : driver2.getWindowHandles()) {
					driver2.switchTo().window(winHandle2);
				}
				
				Thread.sleep(25000);
				driver2.switchTo().frame("swfb_iframe");
				
				title3 = GAUAssignment.getAssignmentName();
				
				Thread.sleep(3000);
				GAUAssignment.clearSW5GAUDate();
				
				Thread.sleep(3000);
				GAUAssignment.unPublishButton();
				
				// Close the new window, if that window no more required
				driver2.close();
				
				// Switch back to original browser (first window)
				driver2.switchTo().window(winHandleBefore2);
				
				Thread.sleep(3000);
				NLTLogin = new LoginPage(driver2);
				NLTLogin.logoutNCIA();
				
				//driver2.close();

			
				//Switch to back again to Student Beta Assignment view and validate 1st Assignment title is not displayed.
				
				Thread.sleep(3000);
				ViewBetaAssignment = new BetaAssignmentPage(driver);
				
				driver.switchTo().window(studentWindowHandle);
				
				Thread.sleep(3000);
				ViewBetaAssignment.validateAssignmentNameNOTExist(title3);
			
				//Logout of NLT application as Student.
				
				NLTLogin = new LoginPage(driver);
				NLTLogin.logoutNLT();
			}
			
			// Closing driver and test.

			@AfterTest
			public void closeTest() throws Exception {

				PropertiesFile.tearDownTest();
				
				wait = new WebDriverWait(driver2,5);
				driver2.close();

			}

}
