
/* Desktop View - Type - Verify that assignments type column sorted and re-sorted A-Z and Z-A based on first and second clicked under 
 * "What's Due" and "Completed or Past Due" table separately. */

package com.wwnorton.NLT.BetaAssignment;

import java.util.Arrays;
import java.util.Collections;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.wwnorton.NLT.BetaAssignment.objectFactory.BetaAssignmentPage;
import com.wwnorton.NLT.BetaAssignment.objectFactory.LaunchBetaAssignment;
import com.wwnorton.NLT.BetaAssignment.objectFactory.LoginPage;
import com.wwnorton.NLT.BetaAssignment.objectFactory.SelectProductDLP;
import com.wwnorton.NLT.BetaAssignment.objectFactory.SetAssignmentGAU;
import com.wwnorton.NLT.BetaAssignment.utilities.GetDate;
import com.wwnorton.NLT.BetaAssignment.utilities.PropertiesFile;
import com.wwnorton.NLT.BetaAssignment.utilities.ReadJsonFile;
import com.wwnorton.NLT.BetaAssignment.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;



//Call to TestNG listeners to save test logs and attachments as screen shots on Test failure.

@Listeners({ TestListener.class })


public class VerifyAssignmentTypesSortingInBetaAssignmentView extends PropertiesFile {
	
	LoginPage NLTLogin;
	LaunchBetaAssignment GoToBetaAssignment;
	BetaAssignmentPage ViewBetaAssignment;
	SetAssignmentGAU GAUAssignment;
	SelectProductDLP ProductDLP;
	
	String userName;
	String Password;
	String LoginName;
	String strCourseName;
	String BetaViewVourseName;
	String jCourseName;
	
	String strGAUDate;
	String jsonProductName;
	String jsonSSName;

	String[] betaAssignmentTypeDueDate_Default;
	String[] betaAssignmentTypePastDate_Default;
	
	
	// TestNG Annotations
			@Parameters("Browser")
			@BeforeTest
			// Call to Properties file initiate Browser and set Test URL.

			public void callPropertiesFile() throws Exception {

				PropertiesFile.readPropertiesFile();
				PropertiesFile.setBrowserConfig();
				PropertiesFile.setURL("psychsci6");

			}

			// Read login data from Json File
			ReadJsonFile readJasonObject = new ReadJsonFile();
			JsonObject jsonobject = readJasonObject.readJson();
			

			// Allure Annotations

			@Severity(SeverityLevel.NORMAL)
			@Description("Verify Assignment type sorting in Beta Assignment View list")
			@Stories("Assignment List")
			
			
			@Test(priority = 0)
			public void TC04_VerifyAssignmentTypesSortingInBetaAssignmentView() throws Exception { 
				
				NLTLogin = new LoginPage(driver);
				
				userName = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("userName").getAsString();
				Password = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("password").getAsString();
				
				NLTLogin.loginNLT(userName, Password);
				
				wait = new WebDriverWait(driver,5);
				wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
				wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));
				
				Thread.sleep(10000);

				LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
				NLTLogin.verifySignin(LoginName, userName);

			
				// Set Assignment GAU Date for InQuizitive Assignment with Due Date and Past Date. 
				
				ProductDLP = new SelectProductDLP(driver);
				
				
				jsonProductName = jsonobject.getAsJsonObject("ProductDLP").get("productName1").getAsString();
				
				Thread.sleep(3000);
				SelectProductDLP.clickProductImage(jsonProductName);
				SelectProductDLP.clickOKButton();
				
				jsonSSName = jsonProductName = jsonobject.getAsJsonObject("StudentSet").get("studentSetName1").getAsString();
				ProductDLP.selectSSByTitle(jsonSSName);
				
				GAUAssignment = new SetAssignmentGAU(driver);
				
				Thread.sleep(3000);
				GAUAssignment.removeAllGAUDates();
				
				String futureDatePlusThirty = GetDate.addDays(30);
				
				Thread.sleep(3000);
				GAUAssignment.setGAUDate(futureDatePlusThirty);
				
				String pastDateMinusThirty = GetDate.addDays(-30);
				
				Thread.sleep(3000);
				GAUAssignment.setGAUDate(pastDateMinusThirty);
				
				Thread.sleep(3000);
				ProductDLP.returnArrowIcon.click();

			
				// Set Assignment GAU Date for ZAPS Assignment with Due Date and Past Date. 
				
				jsonProductName = jsonobject.getAsJsonObject("ProductDLP").get("productName5").getAsString();
				
				Thread.sleep(3000);
				SelectProductDLP.clickProductImage(jsonProductName);
				SelectProductDLP.clickOKButton();
				
				jsonSSName = jsonProductName = jsonobject.getAsJsonObject("StudentSet").get("studentSetName1").getAsString();
				ProductDLP.selectSSByTitle(jsonSSName);
				
				Thread.sleep(3000);
				GAUAssignment.removeAllGAUDates();
				
				String futureDatePlusTwenty = GetDate.addDays(20);
				
				Thread.sleep(3000);
				GAUAssignment.setGAUDate(futureDatePlusTwenty);
				
				String pastDateMinusTwenty = GetDate.addDays(-20);
				
				Thread.sleep(3000);
				GAUAssignment.setGAUDate(pastDateMinusTwenty);
				
				
				//Logout of NCIA application as an Instructor.

				NLTLogin.logoutNCIA();
			
			
				//Login to NLT Application as Student and verify Student is logged in successfully.
				
				NLTLogin = new LoginPage(driver);
				
				userName = jsonobject.getAsJsonObject("StudentLoginCredentials").get("student1").getAsString();
				Password = jsonobject.getAsJsonObject("StudentLoginCredentials").get("password1").getAsString();
				
				Thread.sleep(3000);
				NLTLogin.loginNLT(userName, Password);
				
				//wait = new WebDriverWait(driver,5);
				//wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
				//wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));
				
				Thread.sleep(10000);
				LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
				NLTLogin.verifySignin(LoginName, userName);
					
			
				//Launch Beta Assignment View after selecting Course.

				
				GoToBetaAssignment = new LaunchBetaAssignment(driver);
	
				Thread.sleep(3000);
				GoToBetaAssignment.ClickBetaAssignmentButton();
				
				strCourseName = jsonobject.getAsJsonObject("CourseInformation").get("courseName1").getAsString();
				
				Thread.sleep(3000);
				GoToBetaAssignment.SelectNLTCourse(strCourseName);
					
			
			
				//Validate all assignment icons are displayed in Due and Past Assignment list. 
			
				ViewBetaAssignment = new BetaAssignmentPage(driver);
				
				Thread.sleep(3000);
				ViewBetaAssignment.validateSortIcons_AssignDue();
				
				Thread.sleep(3000);
				ViewBetaAssignment.validateSortIcons_AssignPast();

			
				//Verify Assignment Type sorting and re-sorting under Due Assignment list.

				
				betaAssignmentTypeDueDate_Default = ViewBetaAssignment.getAssignmentTypeList_AssignDue();
				
				Thread.sleep(1000);
				String[] assignmentTypeList_AssignDue = betaAssignmentTypeDueDate_Default;
				Arrays.sort(assignmentTypeList_AssignDue);
				
				ViewBetaAssignment.assignmentTypeSortIcon_BottomArrow_AssignDue.click();
				String[] betaAssignmentTypeDueDate_Asc = ViewBetaAssignment.getAssignmentTypeList_AssignDue();
				
				Assert.assertTrue((betaAssignmentTypeDueDate_Asc[0].equals(assignmentTypeList_AssignDue[0])));
				Assert.assertTrue((betaAssignmentTypeDueDate_Asc[1].equals(assignmentTypeList_AssignDue[1])));
				
				Thread.sleep(1000);
				Arrays.sort(assignmentTypeList_AssignDue, Collections.reverseOrder());
				
				ViewBetaAssignment.assignmentTypeSortIcon_BottomArrow_AssignDue.click();
				String[] betaAssignmentTypeDueDate_Desc = ViewBetaAssignment.getAssignmentTypeList_AssignDue();
				
				Assert.assertTrue((betaAssignmentTypeDueDate_Desc[0].equals(assignmentTypeList_AssignDue[0])));
				Assert.assertTrue((betaAssignmentTypeDueDate_Desc[1].equals(assignmentTypeList_AssignDue[1])));

			
				//Verify Assignment Type sorting and re-sorting under Past Assignment list.
				
				betaAssignmentTypePastDate_Default = ViewBetaAssignment.getAssignmentTypeList_AssignPast();
				
				Thread.sleep(1000);
				String[] assignmentTypeList_AssignPast = betaAssignmentTypePastDate_Default;
				Arrays.sort(assignmentTypeList_AssignPast);
				
				ViewBetaAssignment.assignmentTypeSortIcon_BottomArrow_AssignPast.click();
				String[] betaAssignmentTypePastDate_Asc = ViewBetaAssignment.getAssignmentTypeList_AssignPast();
				
				Assert.assertTrue((betaAssignmentTypePastDate_Asc[0].equals(assignmentTypeList_AssignPast[0])));
				Assert.assertTrue((betaAssignmentTypePastDate_Asc[1].equals(assignmentTypeList_AssignPast[1])));
				
				Thread.sleep(1000);
				Arrays.sort(assignmentTypeList_AssignPast, Collections.reverseOrder());
				
				ViewBetaAssignment.assignmentTypeSortIcon_BottomArrow_AssignPast.click();
				String[] betaAssignmentTypePastDate_Desc = ViewBetaAssignment.getAssignmentTypeList_AssignPast();
				
				Assert.assertTrue((betaAssignmentTypePastDate_Desc[0].equals(assignmentTypeList_AssignPast[0])));
				Assert.assertTrue((betaAssignmentTypePastDate_Desc[1].equals(assignmentTypeList_AssignPast[1])));

				//Logout of NLT application as Student.
				Thread.sleep(2000);
				NLTLogin.logoutNLT();
				
			}
			
			
			// Closing driver and test.

			@AfterTest
			public void closeTest() throws Exception {

				PropertiesFile.tearDownTest();

			}

}


