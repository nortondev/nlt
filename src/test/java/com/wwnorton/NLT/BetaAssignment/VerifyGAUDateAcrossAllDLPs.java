
/* Verify that the GAU set for assignment should be consistent across Student DLP, Beta assignment view 
 * and Instructor DLP when assignment has been set up for DLP having multiple products. */

package com.wwnorton.NLT.BetaAssignment;


import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.wwnorton.NLT.BetaAssignment.objectFactory.BetaAssignmentPage;
import com.wwnorton.NLT.BetaAssignment.objectFactory.LaunchBetaAssignment;
import com.wwnorton.NLT.BetaAssignment.objectFactory.LoginPage;
import com.wwnorton.NLT.BetaAssignment.objectFactory.SelectProductDLP;
import com.wwnorton.NLT.BetaAssignment.objectFactory.SetAssignmentGAU;
import com.wwnorton.NLT.BetaAssignment.utilities.GetDate;
import com.wwnorton.NLT.BetaAssignment.utilities.PropertiesFile;
import com.wwnorton.NLT.BetaAssignment.utilities.ReadJsonFile;
import com.wwnorton.NLT.BetaAssignment.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;



//Call to TestNG listeners to save test logs and attachments as screen shots on Test failure.

@Listeners({ TestListener.class })


public class VerifyGAUDateAcrossAllDLPs extends PropertiesFile {
	
	LoginPage NLTLogin;
	LaunchBetaAssignment GoToBetaAssignment;
	BetaAssignmentPage ViewBetaAssignment;
	SetAssignmentGAU GAUAssignment;
	SelectProductDLP ProductDLP;
	
	String userName;
	String Password;
	String LoginName;
	String strCourseName;
	String BetaViewVourseName;
	String jCourseName;
	
	String jsonProductName1;
	String jsonProductName2;
	String jsonProductName3;
	String jsonSSName;

	String strGAUDate;
	String strTitle1;
	String strTitle2;
	
	String sw5GAUDate1;
	String sw5GAUDate2;
	
	
	// TestNG Annotations
			@Parameters("Browser")
			@BeforeTest
			// Call to Properties file initiate Browser and set Test URL.

			public void callPropertiesFile() throws Exception {

				PropertiesFile.readPropertiesFile();
				PropertiesFile.setBrowserConfig();
				PropertiesFile.setURL("prineco2");
				//PropertiesFile.setNCIAURL_prineco2();

			}

			// Read login data from Json File
			ReadJsonFile readJasonObject = new ReadJsonFile();
			JsonObject jsonobject = readJasonObject.readJson();
			

			// Allure Annotations

			@Severity(SeverityLevel.NORMAL)
			@Description("Verify GAU Date set for assignment should be consistent across DLP's")
			@Stories("Assignment GAU Date")
			
			
			@Test(priority = 0)
			public void TC22_VerifyGAUDateAcrossAllDLPs() throws Exception {
				
				NLTLogin = new LoginPage(driver);
				
				userName = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("userName").getAsString();
				Password = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("password").getAsString();
				
				NLTLogin.loginNLT(userName, Password);
				
				wait = new WebDriverWait(driver,5);
				wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
				wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));
				
				Thread.sleep(10000);

				LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
				NLTLogin.verifySignin(LoginName, userName);

			
				// Remove Assignment GAU Dates for ZAPS.
				
				ProductDLP = new SelectProductDLP(driver);
				
				jsonProductName1 = jsonobject.getAsJsonObject("ProductDLP").get("productName5").getAsString();
							
				Thread.sleep(3000);
				SelectProductDLP.clickProductImage(jsonProductName1);
				SelectProductDLP.clickOKButton();
							
				jsonSSName = jsonobject.getAsJsonObject("StudentSet").get("studentSetName5").getAsString();
				ProductDLP.selectSSByTitle(jsonSSName);
				
				GAUAssignment = new SetAssignmentGAU(driver);
							
				Thread.sleep(3000);
				GAUAssignment.removeAllGAUDates();
				
				Thread.sleep(3000);
				ProductDLP.returnArrowIcon.click();

			
				// Set Assignment GAU Date for InQuizzitive for three Assignments with Future Due Date. 

				
				ProductDLP = new SelectProductDLP(driver);
				
				jsonProductName2 = jsonobject.getAsJsonObject("ProductDLP").get("productName1").getAsString();
				
				Thread.sleep(3000);
				SelectProductDLP.clickProductImage(jsonProductName2);
				SelectProductDLP.clickOKButton();
				
				ProductDLP.selectSSByTitle(jsonSSName);
				
				Thread.sleep(3000);
				GAUAssignment.removeAllGAUDates();
				
				Thread.sleep(3000);
				//Calendar cal1 = Calendar.getInstance();
				//cal1.add(Calendar.DATE, 30);

				//DateFormat dateFormat1 = new SimpleDateFormat("MM/dd/yyyy");		
				//String futureDatePlusThirty =  dateFormat1.format(cal1.getTime());
				
				String futureDatePlusThirty = GetDate.addDays(30);
				
				Thread.sleep(3000);
				String assignmentTitle1 = GAUAssignment.setGAUDate(futureDatePlusThirty);
				String inQuizzitiveDueDate1 = GAUAssignment.getAssignmentGAU(assignmentTitle1);
				
				String futureDatePlusTwenty = GetDate.addDays(20);
						
				Thread.sleep(3000);
				String assignmentTitle2 = GAUAssignment.setGAUDate(futureDatePlusTwenty);
				String inQuizzitiveDueDate2 = GAUAssignment.getAssignmentGAU(assignmentTitle2);
				
				String futureDatePlusTen =  GetDate.addDays(10);
				
				Thread.sleep(3000);
				String assignmentTitle3 = GAUAssignment.setGAUDate(futureDatePlusTen);
				String inQuizzitiveDueDate3 = GAUAssignment.getAssignmentGAU(assignmentTitle3);

				
				Thread.sleep(3000);
				ProductDLP.returnArrowIcon.click();
				
				// Set Assignment GAU Date for SW5 for first Assignment with Future Due Date.
				
				jsonProductName3 = jsonobject.getAsJsonObject("ProductDLP").get("productName2").getAsString();
				
				Thread.sleep(3000);
				SelectProductDLP.clickProductImage(jsonProductName3);
				SelectProductDLP.clickOKButton();
				
				ProductDLP.selectSSByTitle(jsonSSName);
				
				// Store the current window handle
				String winHandleBefore = driver.getWindowHandle();
				
				// Perform the click operation that opens new window
				Thread.sleep(3000);
				GAUAssignment.editAssignment_SetGAU();
				
				for (String winHandle : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);
				}
				
				Thread.sleep(25000);
				driver.switchTo().frame("swfb_iframe");
				
				strTitle1 = GAUAssignment.getAssignmentName();
				
				String futureDatePlusFive =  GetDate.addDays(5);
				
				Thread.sleep(3000);
				GAUAssignment.setSW5GAUDate(futureDatePlusFive);
				
				Thread.sleep(2000);
				GAUAssignment.publishButton();
				
				Thread.sleep(2000);
				GAUAssignment.returnToAssignmentList.click();
				
				// Close the new window, if that window no more required
				driver.close();
				
				// Switch back to original browser (first window)
				driver.switchTo().window(winHandleBefore);
				
				Thread.sleep(3000);
				ProductDLP.returnArrowIcon.click();
				
				SelectProductDLP.clickProductImage(jsonProductName3);
				SelectProductDLP.clickOKButton();
				
				Thread.sleep(2000);
				ProductDLP.selectSSByTitle(jsonSSName);

				Thread.sleep(2000);
				sw5GAUDate1 = GAUAssignment.getGAUDate(strTitle1);
				
				
				// Set Assignment GAU Date for SW5 for second Assignment with Future Due Date.
				
				// Store the current window handle
				String winHandleBefore1 = driver.getWindowHandle();
				
				// Perform the click operation that opens new window
				Thread.sleep(3000);
				GAUAssignment.editAssignment_SetGAU();
				
				for (String winHandle1 : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle1);
				}
				
				Thread.sleep(25000);
				driver.switchTo().frame("swfb_iframe");
				
				strTitle2 = GAUAssignment.getAssignmentName();
				
				String futureDatePlusFifteen = GetDate.addDays(15);
				
				Thread.sleep(3000);
				GAUAssignment.setSW5GAUDate(futureDatePlusFifteen);
				
				Thread.sleep(2000);
				GAUAssignment.publishButton();
				
				Thread.sleep(2000);
				GAUAssignment.returnToAssignmentList.click();
				
				// Close the new window, if that window no more required
				driver.close();
				
				// Switch back to original browser (first window)
				driver.switchTo().window(winHandleBefore1);

				Thread.sleep(3000);
				ProductDLP.returnArrowIcon.click();
				
				SelectProductDLP.clickProductImage(jsonProductName3);
				SelectProductDLP.clickOKButton();
				
				Thread.sleep(2000);
				ProductDLP.selectSSByTitle(jsonSSName);
				
				Thread.sleep(2000);
				sw5GAUDate2 = GAUAssignment.getGAUDate(strTitle2);
				
			
				// Logout of NCIA application as an Instructor.
			
				NLTLogin.logoutNCIA();
			
			
				//Login to NLT Application as Student and verify Student is logged in successfully.
				
				NLTLogin = new LoginPage(driver);
				
				userName = jsonobject.getAsJsonObject("StudentLoginCredentials").get("student2").getAsString();
				Password = jsonobject.getAsJsonObject("StudentLoginCredentials").get("password2").getAsString();
				
				Thread.sleep(3000);
				NLTLogin.loginNLT(userName, Password);
				
				wait = new WebDriverWait(driver,5);
				wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
				wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));
				
				Thread.sleep(10000);
				LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
				NLTLogin.verifySignin(LoginName, userName);
				
				Thread.sleep(3000);
				SelectProductDLP.clickProductImage(jsonProductName2);
				SelectProductDLP.clickOKButton();
				
				Thread.sleep(2000);
				ProductDLP.selectActiveSSByTitle(jsonSSName);
				
				// Validate InQuizitive Assignment Due Date in Student DLP.
				
				Thread.sleep(3000);
				GAUAssignment.validateGAUDate(assignmentTitle1, inQuizzitiveDueDate1);
				GAUAssignment.validateGAUDate(assignmentTitle2, inQuizzitiveDueDate2);
				GAUAssignment.validateGAUDate(assignmentTitle3, inQuizzitiveDueDate3);
				
				Thread.sleep(2000);
				ProductDLP.returnArrowIcon.click();
				
				Thread.sleep(2000);
				SelectProductDLP.clickProductImage(jsonProductName3);
				SelectProductDLP.clickOKButton();
				
				Thread.sleep(2000);
				ProductDLP.selectActiveSSByTitle(jsonSSName);
				
				// Validate Smartwork5 Assignment Due Date in Student DLP.
				
				Thread.sleep(2000);
				GAUAssignment.validateGAUDate(strTitle1, sw5GAUDate1);
				GAUAssignment.validateGAUDate(strTitle2, sw5GAUDate2);
				
				Thread.sleep(2000);
				ProductDLP.returnArrowIcon.click();
				
				//Launch Beta Assignment View after selecting Course.
				
				GoToBetaAssignment = new LaunchBetaAssignment(driver);
	
				Thread.sleep(3000);
				GoToBetaAssignment.ClickBetaAssignmentButton();
				
				strCourseName = jsonobject.getAsJsonObject("CourseInformation").get("courseName5").getAsString();
				
				Thread.sleep(3000);
				GoToBetaAssignment.SelectNLTCourse(strCourseName);
				
				ViewBetaAssignment = new BetaAssignmentPage(driver);
				
				
				// Validate InQuizitive Assignment Due Date in Beta Assignment View.
				
				Thread.sleep(3000);
				ViewBetaAssignment.validateDueDate_InQuizzitive(assignmentTitle1, inQuizzitiveDueDate1);
				ViewBetaAssignment.validateDueDate_InQuizzitive(assignmentTitle2, inQuizzitiveDueDate2);
				ViewBetaAssignment.validateDueDate_InQuizzitive(assignmentTitle3, inQuizzitiveDueDate3);
				
				
				// Validate Smartwork5 Assignment Due Date in Beta Assignment View.
				
				Thread.sleep(2000);
				ViewBetaAssignment.validateDueDate_SmartWork5(strTitle1, sw5GAUDate1);
				ViewBetaAssignment.validateDueDate_SmartWork5(strTitle2, sw5GAUDate2);

				
				//Logout of NLT application as Student.

				NLTLogin.logoutNLT();
				
			}
			

			
			// Closing driver and test.

			@AfterTest
			public void closeTest() throws Exception {

				PropertiesFile.tearDownTest();

			}

}


