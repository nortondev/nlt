
/* Student accesses NLT Student View link from saved bookmark without logged into DLP, 
 * verify that system redirects student to the relevant product DLP page.*/


package com.wwnorton.NLT.BetaAssignment;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.wwnorton.NLT.BetaAssignment.objectFactory.BetaAssignmentPage;
import com.wwnorton.NLT.BetaAssignment.objectFactory.LaunchBetaAssignment;
import com.wwnorton.NLT.BetaAssignment.objectFactory.LoginPage;
import com.wwnorton.NLT.BetaAssignment.objectFactory.SelectProductDLP;
import com.wwnorton.NLT.BetaAssignment.objectFactory.SetAssignmentGAU;
import com.wwnorton.NLT.BetaAssignment.utilities.PropertiesFile;
import com.wwnorton.NLT.BetaAssignment.utilities.ReadJsonFile;
import com.wwnorton.NLT.BetaAssignment.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;



//Call to TestNG listeners to save test logs and attachments as screen shots on Test failure.

@Listeners({ TestListener.class })


public class VerifyRedirectionToDLPLoginPage extends PropertiesFile {
	
	
	LoginPage NLTLogin;
	LaunchBetaAssignment GoToBetaAssignment;
	BetaAssignmentPage ViewBetaAssignment;
	SetAssignmentGAU GAUAssignment;
	SelectProductDLP ProductDLP;
	
	String userName;
	String Password;
	String LoginName;
	String strCourseName;
	String BetaViewVourseName;
	String jCourseName;
	
	String jsonProductName;
	String jsonSSName;
	
	
	// TestNG Annotations
		@Parameters("Browser")
		@BeforeTest
		// Call to Properties file initiate Browser and set Test URL.

		public void callPropertiesFile() throws Exception {

			PropertiesFile.readPropertiesFile();
			PropertiesFile.setBrowserConfig();
			PropertiesFile.setBookMarkURL();

		}

		// Read login data from Json File
		
		ReadJsonFile readJasonObject = new ReadJsonFile();
		JsonObject jsonobject = readJasonObject.readJson();
		

		// Allure Annotations

		@Severity(SeverityLevel.NORMAL)
		@Description("Verify that system redirects user to the relevant product DLP page, "
				+ "when accesses NLT View link without logged into DLP.")
		@Stories("Beta Assignment Link")
		
		
		@Test
		public void TC20_VerifyRedirectionToDLPLoginPage() throws Exception { 
			
			//Verify that Student is redirected to relevant Product DLP page.
			
			NLTLogin = new LoginPage(driver);
			
			NLTLogin.validateDLPPage();
			
			String strProduct = NLTLogin.productTitle.getText();
			String strEdition = NLTLogin.productEdition.getText();
			
			Assert.assertTrue(strProduct.contains("Psychological Science"));
			Assert.assertTrue(strEdition.equalsIgnoreCase("SIXTH EDITION"));
		
			
		}
		
		// Closing driver and test.

		@AfterTest
		public void closeTest() throws Exception {

			PropertiesFile.tearDownTest();

		}
		
}

