

// Verify that eBook is launched successfully when user clicks Open eBook link on Book Details Overlay.


package com.wwnorton.NLT.BetaAssignment;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.wwnorton.NLT.BetaAssignment.objectFactory.LaunchBetaAssignment;
import com.wwnorton.NLT.BetaAssignment.objectFactory.LoginPage;
import com.wwnorton.NLT.BetaAssignment.objectFactory.SelectProductDLP;
import com.wwnorton.NLT.BetaAssignment.objectFactory.eBookDetails;
import com.wwnorton.NLT.BetaAssignment.utilities.PropertiesFile;
import com.wwnorton.NLT.BetaAssignment.utilities.ReadJsonFile;
import com.wwnorton.NLT.BetaAssignment.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;



//Call to TestNG listeners to save test logs and attachments as screen shots on Test failure.

@Listeners({ TestListener.class })



public class VerifyeBookLinkFromBookDetailsOverlay extends PropertiesFile {
	
	LoginPage NLTLogin;
	LaunchBetaAssignment GoToBetaAssignment;
	eBookDetails OpeneBookDetails;
	SelectProductDLP ProductDLP;
	
	String userName;
	String Password;
	String LoginName;
	String strCourseName;
	String  jsonProductName;
	String jsonSSName;
	String eBookURL;

	
	
	// TestNG Annotations
		@Parameters("Browser")
		@BeforeTest
		// Call to Properties file initiate Browser and set Test URL.

		public void callPropertiesFile() throws Exception {

			PropertiesFile.readPropertiesFile();
			PropertiesFile.setBrowserConfig();
			PropertiesFile.setURL("psychsci6");

		}

		// Read login data from Json File
		ReadJsonFile readJasonObject = new ReadJsonFile();
		JsonObject jsonobject = readJasonObject.readJson();
		

		// Allure Annotations

		@Severity(SeverityLevel.NORMAL)
		@Description("Verify eBook related URL is opened in new window and it is matching original eBook URL")
		@Stories("eBook Details")
		

	
		@Test(priority = 0)
		public void TC09_VerifyeBookLinkFromBookDetailsOverlay() throws Exception { 
			
			//Login to NLT Application as Student and verify Student is logged in successfully.
			NLTLogin = new LoginPage(driver);
			
			userName = jsonobject.getAsJsonObject("StudentLoginCredentials").get("student1").getAsString();
			Password = jsonobject.getAsJsonObject("StudentLoginCredentials").get("password1").getAsString();
			
			Thread.sleep(3000);
			NLTLogin.loginNLT(userName, Password);
			
			wait = new WebDriverWait(driver,5);
			wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
			wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));
			
			Thread.sleep(3000);
			LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
			NLTLogin.verifySignin(LoginName, userName);
		
		
			//Get eBook URL from DLP Page

			OpeneBookDetails = new eBookDetails(driver);
			
			jsonProductName = jsonobject.getAsJsonObject("ProductDLP").get("productName3").getAsString();
			
			ProductDLP = new SelectProductDLP(driver);
			
			Thread.sleep(3000);
			SelectProductDLP.clickProductImage(jsonProductName);
			
			Thread.sleep(2000);
			// Store the current window handle
			String winHandleBefore = driver.getWindowHandle();
			
			SelectProductDLP.clickOKButton();
			
			jsonSSName = jsonProductName = jsonobject.getAsJsonObject("StudentSet").get("studentSetName1").getAsString();
			
			Thread.sleep(2000);
			ProductDLP.selectActiveSSByTitle(jsonSSName);
			
			eBookURL = OpeneBookDetails.captureeBookURL_DLP(jsonProductName);
			
			// Switch back to original browser (first window)
			driver.switchTo().window(winHandleBefore);
			
			wait.until(ExpectedConditions.visibilityOf(eBookDetails.returnToDLP));
			eBookDetails.returnToDLP.click();
		
		
			//Launch Beta Assignment View after selecting Course.
			
			GoToBetaAssignment = new LaunchBetaAssignment(driver);
			
			Thread.sleep(2000);
			GoToBetaAssignment.ClickBetaAssignmentButton();
			
			strCourseName = jsonobject.getAsJsonObject("CourseInformation").get("courseName1").getAsString();
			Thread.sleep(2000);
			GoToBetaAssignment.SelectNLTCourse(strCourseName);
		
		
			//Verify eBook opens in new tab and validate eBook URL as a Student.
			
			// Store the current window handle
				String winHandleBefore1 = driver.getWindowHandle();
						
				OpeneBookDetails = new eBookDetails(driver);
				Thread.sleep(2000);
				OpeneBookDetails.clickMoreInfoButton();
				
				Thread.sleep(2000);
				OpeneBookDetails.clickOpeneBookButton();
		
					
			for (String winHandle : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);
				}
			
				Thread.sleep(2000);
				String url = driver.getCurrentUrl();
				String bookID = url.substring(url.lastIndexOf("/") + 1);
		        System.out.println(bookID); 
				
				Assert.assertEquals(url.equals(eBookURL), true);
					
					
			// Close the new window, if that window no more required
				driver.close();
					
			// Switch back to original browser (first window)
				driver.switchTo().window(winHandleBefore1);
				
				Thread.sleep(2000);
				OpeneBookDetails.closeeBookDetailsOverlayModal.click();

		
			//Logout of NLT application as Student.
			Thread.sleep(2000);	
			NLTLogin.logoutNLT();
		
		
			//Login to NLT application as an Instructor.
			
			NLTLogin = new LoginPage(driver);
			
			userName = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("userName").getAsString();
			Password = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("password").getAsString();
			
			NLTLogin.loginNLT(userName, Password);
			
			wait = new WebDriverWait(driver,5);
			wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
			wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));

			LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
			NLTLogin.verifySignin(LoginName, userName);
		
		
			//Launch Beta Assignment View after selecting Course.
			
			GoToBetaAssignment = new LaunchBetaAssignment(driver);
			Thread.sleep(2000);
			GoToBetaAssignment.ClickBetaAssignmentButton();
			
			strCourseName = jsonobject.getAsJsonObject("CourseInformation").get("courseName1").getAsString();
			Thread.sleep(2000);
			GoToBetaAssignment.SelectNLTCourse(strCourseName);
		
		//Verify eBook opens in new tab and validate eBook URL as an Instructor.
						
			// Store the current window handle
			String winHandleBefore2 = driver.getWindowHandle();
							
			OpeneBookDetails = new eBookDetails(driver);
			
			Thread.sleep(2000);
			OpeneBookDetails.clickMoreInfoButton();
			
			Thread.sleep(2000);
			OpeneBookDetails.clickOpeneBookButton();
			
						
			for (String winHandle : driver.getWindowHandles()) {
						driver.switchTo().window(winHandle);
				}
				
			Thread.sleep(2000);
			String url1 = driver.getCurrentUrl();
			String bookID2 = url1.substring(url.lastIndexOf("/") + 1);
	        System.out.println(bookID2); 
					
			Assert.assertEquals(url.equals(eBookURL), true);
						
						
			// Close the new window, if that window no more required
				driver.close();
						
			// Switch back to original browser (first window)
				driver.switchTo().window(winHandleBefore2);
			
			Thread.sleep(2000);
			OpeneBookDetails.closeeBookDetailsOverlayModal.click();

		
		//Logout of NLT application as an Instructor.
			
			Thread.sleep(2000);
			NLTLogin.logoutNLT();
		}
		

		
		// Closing driver and test.

		@AfterTest
		public void closeTest() throws Exception {

			PropertiesFile.tearDownTest();

		}
		
}
