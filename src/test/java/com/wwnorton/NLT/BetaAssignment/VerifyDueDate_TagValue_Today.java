

/* Verify that user will be able to see Due date tag value as "Today" when while creating assignment due date 
 * is set as of the same day under "What's Due" assignment section. */


package com.wwnorton.NLT.BetaAssignment;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.wwnorton.NLT.BetaAssignment.objectFactory.BetaAssignmentPage;
import com.wwnorton.NLT.BetaAssignment.objectFactory.LaunchBetaAssignment;
import com.wwnorton.NLT.BetaAssignment.objectFactory.LoginPage;
import com.wwnorton.NLT.BetaAssignment.objectFactory.SelectProductDLP;
import com.wwnorton.NLT.BetaAssignment.objectFactory.SetAssignmentGAU;
import com.wwnorton.NLT.BetaAssignment.utilities.PropertiesFile;
import com.wwnorton.NLT.BetaAssignment.utilities.ReadJsonFile;
import com.wwnorton.NLT.BetaAssignment.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;


//Call to TestNG listeners to save test logs and attachments as screen shots on Test failure.

@Listeners({ TestListener.class })


public class VerifyDueDate_TagValue_Today extends PropertiesFile {
	
	LoginPage NLTLogin;
	LaunchBetaAssignment GoToBetaAssignment;
	BetaAssignmentPage ViewBetaAssignment;
	SelectProductDLP ProductDLP;
	SetAssignmentGAU GAUAssignment;
	
	String userName;
	String Password;
	String LoginName;
	String strCourseName;
	String jCourseName;
	
	String jsonProductName;
	String jsonSSName;
	String currentGAUDate;
	
	
	// TestNG Annotations
		@Parameters("Browser")
		@BeforeTest
		// Call to Properties file initiate Browser and set Test URL.

		public void callPropertiesFile() throws Exception {

			PropertiesFile.readPropertiesFile();
			PropertiesFile.setBrowserConfig();
			PropertiesFile.setURL("psychsci6");

		}

		// Read login data from Json File
		ReadJsonFile readJasonObject = new ReadJsonFile();
		JsonObject jsonobject = readJasonObject.readJson();
		

		// Allure Annotations

		@Severity(SeverityLevel.NORMAL)
		@Description("Verify that Due date tag value is displayed as 'Today' under What's Due in Assignment list.")
		@Stories("Assignment List")
		
		
		@Test(priority = 0)
		public void TC16_VerifyDueDate_TagValue_Today() throws Exception { 
			
			//Login to NCIA application as Instructor.
			
			NLTLogin = new LoginPage(driver);
			
			userName = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("userName").getAsString();
			Password = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("password").getAsString();
			
			NLTLogin.loginNLT(userName, Password);
			
			wait = new WebDriverWait(driver,5);
			wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
			wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));

			LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
			NLTLogin.verifySignin(LoginName, userName);
			
		
			//Set Assignment Date as Current GAU Date.
			
			ProductDLP = new SelectProductDLP(driver);
			
			
			jsonProductName = jsonobject.getAsJsonObject("ProductDLP").get("productName1").getAsString();
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			currentGAUDate = dateFormat.format(new Date());
			
			Thread.sleep(3000);
			SelectProductDLP.clickProductImage(jsonProductName);
			SelectProductDLP.clickOKButton();
			
			jsonSSName = jsonProductName = jsonobject.getAsJsonObject("StudentSet").get("studentSetName1").getAsString();
			ProductDLP.selectSSByTitle(jsonSSName);
			
			GAUAssignment = new SetAssignmentGAU(driver);
			
			Thread.sleep(3000);
			GAUAssignment.removeAllGAUDates();
			
			Thread.sleep(3000);
			GAUAssignment.setGAUDate(currentGAUDate);
			

			//Logout of NCIA application as an Instructor.
			NLTLogin.logoutNCIA();
		
		
			//Login to NLT Application as Student and verify Student is logged in successfully.
					
			NLTLogin = new LoginPage(driver);
					
			userName = jsonobject.getAsJsonObject("StudentLoginCredentials").get("student1").getAsString();
			Password = jsonobject.getAsJsonObject("StudentLoginCredentials").get("password1").getAsString();
					
			Thread.sleep(3000);
			NLTLogin.loginNLT(userName, Password);
					
			wait = new WebDriverWait(driver,5);
			wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
			wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));
					
			Thread.sleep(3000);
			LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
			NLTLogin.verifySignin(LoginName, userName);

				
			//Launch Beta Assignment View after selecting Course.
					
			GoToBetaAssignment = new LaunchBetaAssignment(driver);
			Thread.sleep(3000);
			GoToBetaAssignment.ClickBetaAssignmentButton();
					
			strCourseName = jsonobject.getAsJsonObject("CourseInformation").get("courseName1").getAsString();
			Thread.sleep(3000);
			GoToBetaAssignment.SelectNLTCourse(strCourseName);
						
		
		
			//Verify Current day is displayed under Assignment Due in Assignment List. 
					
			Thread.sleep(3000);
			ViewBetaAssignment = new BetaAssignmentPage(driver);
			 String strCurrentDay = ViewBetaAssignment.currentDay.getText();
			 Assert.assertEquals(strCurrentDay, "Today");
			 
		
		
			// Logout of NLT application as Student.
			NLTLogin.logoutNLT();
		
		}
		
		// Closing driver and test.

		@AfterTest
		public void closeTest() throws Exception {

			PropertiesFile.tearDownTest();

		}
		
}
