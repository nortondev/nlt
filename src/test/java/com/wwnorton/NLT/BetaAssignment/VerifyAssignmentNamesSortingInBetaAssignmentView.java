
/* Student Beta View - Verify that user can sort the Assignment names under 
 * "What's Due" and "Completed or Past Due" table separately. */

package com.wwnorton.NLT.BetaAssignment;

import java.util.Arrays;
import java.util.Collections;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.wwnorton.NLT.BetaAssignment.objectFactory.BetaAssignmentPage;
import com.wwnorton.NLT.BetaAssignment.objectFactory.LaunchBetaAssignment;
import com.wwnorton.NLT.BetaAssignment.objectFactory.LoginPage;
import com.wwnorton.NLT.BetaAssignment.objectFactory.SelectProductDLP;
import com.wwnorton.NLT.BetaAssignment.objectFactory.SetAssignmentGAU;
import com.wwnorton.NLT.BetaAssignment.utilities.GetDate;
import com.wwnorton.NLT.BetaAssignment.utilities.PropertiesFile;
import com.wwnorton.NLT.BetaAssignment.utilities.ReadJsonFile;
import com.wwnorton.NLT.BetaAssignment.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;



//Call to TestNG listeners to save test logs and attachments as screen shots on Test failure.

@Listeners({ TestListener.class })


public class VerifyAssignmentNamesSortingInBetaAssignmentView extends PropertiesFile {
	
	LoginPage NLTLogin;
	LaunchBetaAssignment GoToBetaAssignment;
	BetaAssignmentPage ViewBetaAssignment;
	SetAssignmentGAU GAUAssignment;
	SelectProductDLP ProductDLP;
	
	String userName;
	String Password;
	String LoginName;
	String strCourseName;
	String BetaViewVourseName;
	String jCourseName;
	
	String strGAUDate;
	String jsonProductName;
	String jsonSSName;

	String[] betaAssignmentNameDueDate_Default;
	String[] betaAssignmentNamePastDate_Default;
	
	
	// TestNG Annotations
			@Parameters("Browser")
			@BeforeTest
			// Call to Properties file initiate Browser and set Test URL.

			public void callPropertiesFile() throws Exception {

				PropertiesFile.readPropertiesFile();
				PropertiesFile.setBrowserConfig();
				PropertiesFile.setURL("psychsci6");

			}

			// Read login data from Json File
			ReadJsonFile readJasonObject = new ReadJsonFile();
			JsonObject jsonobject = readJasonObject.readJson();
			

			// Allure Annotations

			@Severity(SeverityLevel.NORMAL)
			@Description("Verify Assignment names sorting in Beta Assignment View list")
			@Stories("Assignment List")
			
			
			@Test(priority = 0)
			public void TC03_VerifyAssignmentNamesSortingInBetaAssignmentView() throws Exception { 
				
				NLTLogin = new LoginPage(driver);
				
				userName = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("userName").getAsString();
				Password = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("password").getAsString();
				
				NLTLogin.loginNLT(userName, Password);
				
				wait = new WebDriverWait(driver,5);
				wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
				wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));
				
				Thread.sleep(10000);

				LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
				NLTLogin.verifySignin(LoginName, userName);

			
				// Set Assignment GAU Date for ZAPS Assignment with Due Date and Past Date.
				
				ProductDLP = new SelectProductDLP(driver);
				
				jsonProductName = jsonobject.getAsJsonObject("ProductDLP").get("productName5").getAsString();
							
				Thread.sleep(3000);
				SelectProductDLP.clickProductImage(jsonProductName);
				SelectProductDLP.clickOKButton();
							
				jsonSSName = jsonProductName = jsonobject.getAsJsonObject("StudentSet").get("studentSetName1").getAsString();
				ProductDLP.selectSSByTitle(jsonSSName);
				
				GAUAssignment = new SetAssignmentGAU(driver);
							
				Thread.sleep(3000);
				GAUAssignment.removeAllGAUDates();
				
				Thread.sleep(3000);
				ProductDLP.returnArrowIcon.click();

			
				// Set Assignment GAU Date for three Assignments each for Due Date and Past Date. 

				
				ProductDLP = new SelectProductDLP(driver);
				
				jsonProductName = jsonobject.getAsJsonObject("ProductDLP").get("productName1").getAsString();
				
				String futureDatePlusThirty = GetDate.addDays(30);
				String pastDateMinusThirty = GetDate.addDays(-30);
				
				Thread.sleep(3000);
				SelectProductDLP.clickProductImage(jsonProductName);
				SelectProductDLP.clickOKButton();
				
				jsonSSName = jsonProductName = jsonobject.getAsJsonObject("StudentSet").get("studentSetName1").getAsString();
				ProductDLP.selectSSByTitle(jsonSSName);
				
				GAUAssignment = new SetAssignmentGAU(driver);
				
				Thread.sleep(3000);
				GAUAssignment.removeAllGAUDates();
				
				Thread.sleep(3000);
				GAUAssignment.setGAUDateAll(futureDatePlusThirty);
				
				Thread.sleep(3000);
				GAUAssignment.setGAUDateAll(pastDateMinusThirty);
				
			
				// Logout of NCIA application as an Instructor.
			
				NLTLogin.logoutNCIA();
			
			
				//Login to NLT Application as Student and verify Student is logged in successfully.
				
				NLTLogin = new LoginPage(driver);
				
				userName = jsonobject.getAsJsonObject("StudentLoginCredentials").get("student1").getAsString();
				Password = jsonobject.getAsJsonObject("StudentLoginCredentials").get("password1").getAsString();
				
				Thread.sleep(3000);
				NLTLogin.loginNLT(userName, Password);
				
				wait = new WebDriverWait(driver,5);
				wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
				wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));
				
				Thread.sleep(10000);
				LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
				NLTLogin.verifySignin(LoginName, userName);

			
				//Launch Beta Assignment View after selecting Course.
				
				GoToBetaAssignment = new LaunchBetaAssignment(driver);
	
				Thread.sleep(3000);
				GoToBetaAssignment.ClickBetaAssignmentButton();
				
				strCourseName = jsonobject.getAsJsonObject("CourseInformation").get("courseName1").getAsString();
				
				Thread.sleep(3000);
				GoToBetaAssignment.SelectNLTCourse(strCourseName);
			
			
				//Validate all assignment icons are displayed in Due and Past Assignment list.
			
				ViewBetaAssignment = new BetaAssignmentPage(driver);
				
				Thread.sleep(3000);
				ViewBetaAssignment.validateSortIcons_AssignDue();
				
				Thread.sleep(3000);
				ViewBetaAssignment.validateSortIcons_AssignPast();

			
				//Verify Assignment Name sorting and re-sorting under Due Assignment list.
				
				betaAssignmentNameDueDate_Default = ViewBetaAssignment.getAssignmentList_AssignDue();
				
				Thread.sleep(1000);
				String[] assignmentList_AssignDue = betaAssignmentNameDueDate_Default;
				Arrays.sort(assignmentList_AssignDue);
				
				ViewBetaAssignment.assignmentNameSortIcon_BottomArrow_AssignDue.click();
				String[] betaAssignmentNameDueDate_Asc = ViewBetaAssignment.getAssignmentList_AssignDue();
				
				Assert.assertTrue((betaAssignmentNameDueDate_Asc[0].equals(assignmentList_AssignDue[0])));
				Assert.assertTrue((betaAssignmentNameDueDate_Asc[1].equals(assignmentList_AssignDue[1])));
				Assert.assertTrue((betaAssignmentNameDueDate_Asc[2].equals(assignmentList_AssignDue[2])));
				
				Thread.sleep(1000);
				Arrays.sort(assignmentList_AssignDue, Collections.reverseOrder());
				
				ViewBetaAssignment.assignmentNameSortIcon_BottomArrow_AssignDue.click();
				String[] betaAssignmentNameDueDate_Desc = ViewBetaAssignment.getAssignmentList_AssignDue();
				
				Assert.assertTrue((betaAssignmentNameDueDate_Desc[0].equals(assignmentList_AssignDue[0])));
				Assert.assertTrue((betaAssignmentNameDueDate_Desc[1].equals(assignmentList_AssignDue[1])));
				Assert.assertTrue((betaAssignmentNameDueDate_Desc[2].equals(assignmentList_AssignDue[2])));

			
				//Verify Assignment Name sorting and re-sorting under Past Assignment list. 

				betaAssignmentNamePastDate_Default = ViewBetaAssignment.getAssignmentList_AssignPast();
				
				Thread.sleep(1000);
				String[] assignmentList_AssignPast = betaAssignmentNamePastDate_Default;
				Arrays.sort(assignmentList_AssignPast);
				
				ViewBetaAssignment.assignmentNameSortIcon_BottomArrow_AssignPast.click();
				String[] betaAssignmentNamePastDate_Asc = ViewBetaAssignment.getAssignmentList_AssignPast();
				
				Assert.assertTrue((betaAssignmentNamePastDate_Asc[0].equals(assignmentList_AssignPast[0])));
				Assert.assertTrue((betaAssignmentNamePastDate_Asc[1].equals(assignmentList_AssignPast[1])));
				Assert.assertTrue((betaAssignmentNamePastDate_Asc[2].equals(assignmentList_AssignPast[2])));
				
				Thread.sleep(1000);
				Arrays.sort(assignmentList_AssignPast, Collections.reverseOrder());
				
				ViewBetaAssignment.assignmentNameSortIcon_BottomArrow_AssignPast.click();
				String[] betaAssignmentNamePastDate_Desc = ViewBetaAssignment.getAssignmentList_AssignPast();
				
				Assert.assertTrue((betaAssignmentNamePastDate_Desc[0].equals(assignmentList_AssignPast[0])));
				Assert.assertTrue((betaAssignmentNamePastDate_Desc[1].equals(assignmentList_AssignPast[1])));
				Assert.assertTrue((betaAssignmentNamePastDate_Desc[2].equals(assignmentList_AssignPast[2])));

				
				//Logout of NLT application as Student.

				NLTLogin.logoutNLT();
				
			}
			

			
			// Closing driver and test.

			@AfterTest
			public void closeTest() throws Exception {

				PropertiesFile.tearDownTest();

			}

}

