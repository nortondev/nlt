
//Verify that HTML formatting is rendered in the “DLP Bottom Div” element.

package com.wwnorton.NLT.BetaAssignment;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.wwnorton.NLT.BetaAssignment.objectFactory.BetaAssignmentPage;
import com.wwnorton.NLT.BetaAssignment.objectFactory.LaunchBetaAssignment;
import com.wwnorton.NLT.BetaAssignment.objectFactory.LoginPage;
import com.wwnorton.NLT.BetaAssignment.objectFactory.ProductConfig_PCAT;
import com.wwnorton.NLT.BetaAssignment.objectFactory.eBookDetails;
import com.wwnorton.NLT.BetaAssignment.utilities.PropertiesFile;
import com.wwnorton.NLT.BetaAssignment.utilities.ReadJsonFile;
import com.wwnorton.NLT.BetaAssignment.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;



//Call to TestNG listeners to save test logs and attachments as screen shots on Test failure.

@Listeners({ TestListener.class })


public class VerifyHTMLFormatting_DLPDivBottom extends PropertiesFile {
	
	LoginPage NLTLogin;
	ProductConfig_PCAT ProductDetails;
	LaunchBetaAssignment GoToBetaAssignment;
	BetaAssignmentPage ViewBetaAssignment;
	eBookDetails MoreInfo;
	
	String userName;
	String Password;
	String LoginName;
	String strCourseName;
	String BetaViewVourseName;
	String jCourseName;

	boolean eBookBottomDivExist;
	
	
	// TestNG Annotations
		@Parameters("Browser")
		@BeforeTest
		// Call to Properties file initiate Browser and set Test URL.

		public void callPropertiesFile() throws Exception {

			PropertiesFile.readPropertiesFile();
			PropertiesFile.setBrowserConfig();
			PropertiesFile.setPCATURL();

		}

		// Read login data from Json File
		ReadJsonFile readJasonObject = new ReadJsonFile();
		JsonObject jsonobject = readJasonObject.readJson();
		

		// Allure Annotations

		@Severity(SeverityLevel.NORMAL)
		@Description("Verify HTML formatting is rendered correctly for DLP Bottom Div” element")
		@Stories("eBook Details")
		
		
		
		@Test(priority = 0)
		public void TC08_VerifyHTMLFormatting_DLPDivBottom() throws Exception { 
			
			//Login to PCAT Application as Admin user.
			
			NLTLogin = new LoginPage(driver);
			
			userName = jsonobject.getAsJsonObject("AdminLoginCredentials").get("userName").getAsString();
			Password = jsonobject.getAsJsonObject("AdminLoginCredentials").get("password").getAsString();
			
			NLTLogin.loginNLT(userName, Password);
			
			wait = new WebDriverWait(driver,5);
			wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
			wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));

		
			//Capture all Product Information in PCAT application.
			
			ProductDetails = new ProductConfig_PCAT(driver);
			
			Thread.sleep(2000);
			ProductDetails.selectDiscipline("52f015097e149d83af166255");
			Thread.sleep(2000);
			ProductDetails.selectProduct("535703a00a58df6f0431055c");
			
			Thread.sleep(2000);
			ProductDetails.editProductInfoButton.click();
			
			ProductDetails.removeProductTitle();
			ProductDetails.editProductTitle.sendKeys("Psychological Science");
			
			ProductDetails.removeProductEdition();
			ProductDetails.editProductEdition.sendKeys("Fifth Edition");
			
			ProductDetails.removeProductAuthor();
			ProductDetails.editProductAuthor.sendKeys("Gazzaniga and Halpern");
			
			ProductDetails.removeProductDLPBottomDiv();
			ProductDetails.editDLPBottomDiv.sendKeys("<b>Psychsci5</b>");
			
			ProductDetails.saveProductInfoButton.click();
			
			
			Thread.sleep(2000);
			ProductDetails.signOutPCAT();
		
		
			//Login to NLT Application as Student and verify Student is logged in successfully.
				
			Thread.sleep(2000);
			PropertiesFile.setURL("psychsci5");
					
			NLTLogin = new LoginPage(driver);
					
			userName = jsonobject.getAsJsonObject("StudentLoginCredentials").get("student1").getAsString();
			Password = jsonobject.getAsJsonObject("StudentLoginCredentials").get("password1").getAsString();
					
			Thread.sleep(3000);
			NLTLogin.loginNLT(userName, Password);
					
			wait = new WebDriverWait(driver,5);
			wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
			wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));
					
			Thread.sleep(3000);
			LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
			NLTLogin.verifySignin(LoginName, userName);

			
			//Launch Beta Assignment View after selecting Course.
				
			GoToBetaAssignment = new LaunchBetaAssignment(driver);
			Thread.sleep(3000);
			GoToBetaAssignment.ClickBetaAssignmentButton();
				
			strCourseName = jsonobject.getAsJsonObject("CourseInformation").get("courseName7").getAsString();
			Thread.sleep(3000);
			GoToBetaAssignment.SelectNLTCourse(strCourseName);
				
		
			//Capture Book Information from book Details Overlay.
						
			MoreInfo = new eBookDetails(driver);
				
			Thread.sleep(2000);
			MoreInfo.clickMoreInfoButton();
				
			WebElement eBookBottomDiv;
			Thread.sleep(2000);
				
				try {
						eBookBottomDiv = driver.findElement(By.xpath("//div[@class='ebook-info-lowerdiv']/b"));
						eBookBottomDivExist = eBookBottomDiv.isDisplayed();
				}
		
				catch (NoSuchElementException e) 
				{

					eBookBottomDivExist = false;
			
				}
		
				catch (Exception e) {
				System.out.println("Exception Handlled: " + e.getMessage());
	
				}
				
				Assert.assertEquals(eBookBottomDivExist, true);
				
			Thread.sleep(2000);
			MoreInfo.closeeBookDetailsOverlayModal.click();
				
		
			// Logout of NLT application as Student.
			Thread.sleep(2000);
			NLTLogin.logoutNLT();
		
		
			//Login to NLT application as an Instructor.
					
			NLTLogin = new LoginPage(driver);
					
			userName = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("userName").getAsString();
			Password = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("password").getAsString();
					
			NLTLogin.loginNLT(userName, Password);
					
			wait = new WebDriverWait(driver,5);
			wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
			wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));

			LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
			NLTLogin.verifySignin(LoginName, userName);
				
					
			//Launch Beta Assignment View after selecting Course.
					
			GoToBetaAssignment = new LaunchBetaAssignment(driver);
			Thread.sleep(2000);
			GoToBetaAssignment.ClickBetaAssignmentButton();
					
			strCourseName = jsonobject.getAsJsonObject("CourseInformation").get("courseName7").getAsString();
			Thread.sleep(2000);
			GoToBetaAssignment.SelectNLTCourse(strCourseName);
						
				
			//Capture Book Information from book Details Overlay.
			
				MoreInfo = new eBookDetails(driver);
				
				Thread.sleep(3000);
				MoreInfo.clickMoreInfoButton();
						
				//WebElement eBookBottomDiv;
				Thread.sleep(3000);
				
				try {
						eBookBottomDiv = driver.findElement(By.xpath("//div[@class='ebook-info-lowerdiv']/b"));
						eBookBottomDivExist = eBookBottomDiv.isDisplayed();
				}
		
				catch (NoSuchElementException e) 
				{

					eBookBottomDivExist = false;
			
				}
		
				catch (Exception e) {
				System.out.println("Exception Handlled: " + e.getMessage());
	
				}
				
				Thread.sleep(3000);
				Assert.assertEquals(eBookBottomDivExist, true);
				
				MoreInfo.closeeBookDetailsOverlayModal.click();

		
			//Logout of NLT application as an Instructor.
				
			Thread.sleep(2000);
			NLTLogin.logoutNLT();
		
		}
		
		
		// Closing driver and test.

		@AfterTest
		public void closeTest() throws Exception {

			PropertiesFile.tearDownTest();

		}
		
}

