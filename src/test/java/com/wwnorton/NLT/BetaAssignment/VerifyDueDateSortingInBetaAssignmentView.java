
/* Desktop- Due Date - Verify that Due dates are sorted and re-sorted as on first click 
 * is earliest to latest, second click is latest to earliest. */

package com.wwnorton.NLT.BetaAssignment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.wwnorton.NLT.BetaAssignment.objectFactory.BetaAssignmentPage;
import com.wwnorton.NLT.BetaAssignment.objectFactory.LaunchBetaAssignment;
import com.wwnorton.NLT.BetaAssignment.objectFactory.LoginPage;
import com.wwnorton.NLT.BetaAssignment.objectFactory.SelectProductDLP;
import com.wwnorton.NLT.BetaAssignment.objectFactory.SetAssignmentGAU;
import com.wwnorton.NLT.BetaAssignment.utilities.GetDate;
import com.wwnorton.NLT.BetaAssignment.utilities.PropertiesFile;
import com.wwnorton.NLT.BetaAssignment.utilities.ReadJsonFile;
import com.wwnorton.NLT.BetaAssignment.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;



//Call to TestNG listeners to save test logs and attachments as screen shots on Test failure.

@Listeners({ TestListener.class })


public class VerifyDueDateSortingInBetaAssignmentView extends PropertiesFile {
	
	LoginPage NLTLogin;
	LaunchBetaAssignment GoToBetaAssignment;
	BetaAssignmentPage ViewBetaAssignment;
	SetAssignmentGAU GAUAssignment;
	SelectProductDLP ProductDLP;
	
	String userName;
	String Password;
	String LoginName;
	String strCourseName;
	String BetaViewVourseName;
	String jCourseName;
	
	String strGAUDate;
	String jsonProductName;
	String jsonSSName;

	List<Date> betaAssignmentDueDate_Default;
	List<Date> betaAssignmentPastDate_Default;
	
	
	// TestNG Annotations
			@Parameters("Browser")
			@BeforeTest
			// Call to Properties file initiate Browser and set Test URL.

			public void callPropertiesFile() throws Exception {

				PropertiesFile.readPropertiesFile();
				PropertiesFile.setBrowserConfig();
				PropertiesFile.setURL("psychsci6");

			}

			// Read login data from Json File
			ReadJsonFile readJasonObject = new ReadJsonFile();
			JsonObject jsonobject = readJasonObject.readJson();
			

			// Allure Annotations

			@Severity(SeverityLevel.NORMAL)
			@Description("Verify Assignment Due Date sorting in Beta Assignment View list")
			@Stories("Assignment List")
			
		
			@Test(priority = 0)
			public void TC05_VerifyDueDateSortingInBetaAssignmentView() throws Exception { 
				
				//Login to NCIA application as an Instructor.
				
				NLTLogin = new LoginPage(driver);
				
				userName = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("userName").getAsString();
				Password = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("password").getAsString();
				
				NLTLogin.loginNLT(userName, Password);
				
				wait = new WebDriverWait(driver,5);
				wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
				wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));
				
				Thread.sleep(10000);

				LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
				NLTLogin.verifySignin(LoginName, userName);

			
				// Set Assignment GAU Date for InQuizitive Assignment with Due Date and Past Date. 

				ProductDLP = new SelectProductDLP(driver);
				
				
				jsonProductName = jsonobject.getAsJsonObject("ProductDLP").get("productName1").getAsString();
				
				Thread.sleep(3000);
				SelectProductDLP.clickProductImage(jsonProductName);
				SelectProductDLP.clickOKButton();
				
				jsonSSName = jsonProductName = jsonobject.getAsJsonObject("StudentSet").get("studentSetName1").getAsString();
				ProductDLP.selectSSByTitle(jsonSSName);
				
				GAUAssignment = new SetAssignmentGAU(driver);
				
				Thread.sleep(3000);
				GAUAssignment.removeAllGAUDates();
				
				String futureDatePlusThirty = GetDate.addDays(30);
				
				Thread.sleep(3000);
				GAUAssignment.setGAUDate(futureDatePlusThirty);
				
				String futureDatePlusTwenty = GetDate.addDays(20);
				
				Thread.sleep(3000);
				GAUAssignment.setGAUDate(futureDatePlusTwenty);

				String pastDateMinusThirty = GetDate.addDays(-30);
				
				Thread.sleep(3000);
				GAUAssignment.setGAUDate(pastDateMinusThirty);
				
				String pastDateMinusFifteen = GetDate.addDays(-15);
				
				Thread.sleep(3000);
				GAUAssignment.setGAUDate(pastDateMinusFifteen);
				
				Thread.sleep(3000);
				ProductDLP.returnArrowIcon.click();

			
				// Set Assignment GAU Date for ZAPS Assignment with Due Date and Past Date. 	
				
				jsonProductName = jsonobject.getAsJsonObject("ProductDLP").get("productName5").getAsString();
				
				Thread.sleep(3000);
				SelectProductDLP.clickProductImage(jsonProductName);
				SelectProductDLP.clickOKButton();
				
				jsonSSName = jsonProductName = jsonobject.getAsJsonObject("StudentSet").get("studentSetName1").getAsString();
				ProductDLP.selectSSByTitle(jsonSSName);
				
				Thread.sleep(3000);
				GAUAssignment.removeAllGAUDates();
				
				String futureDatePlusTen = GetDate.addDays(10);
				
				Thread.sleep(3000);
				GAUAssignment.setGAUDate(futureDatePlusTen);
				
				String pastDateMinusTen = GetDate.addDays(-10);
				
				Thread.sleep(3000);
				GAUAssignment.setGAUDate(pastDateMinusTen);
			

				// Logout of NCIA application as an Instructor.
				NLTLogin.logoutNCIA();
			
			
				//Login to NLT Application as Student and verify Student is logged in successfully.
				
				NLTLogin = new LoginPage(driver);
				
				userName = jsonobject.getAsJsonObject("StudentLoginCredentials").get("student1").getAsString();
				Password = jsonobject.getAsJsonObject("StudentLoginCredentials").get("password1").getAsString();
				
				Thread.sleep(3000);
				NLTLogin.loginNLT(userName, Password);
				
				wait = new WebDriverWait(driver,5);
				wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
				wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));
				
				Thread.sleep(10000);
				LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
				NLTLogin.verifySignin(LoginName, userName);
					
				
				//Launch Beta Assignment View after selecting Course.
				
				GoToBetaAssignment = new LaunchBetaAssignment(driver);
	
				Thread.sleep(3000);
				GoToBetaAssignment.ClickBetaAssignmentButton();
				
				strCourseName = jsonobject.getAsJsonObject("CourseInformation").get("courseName1").getAsString();
				
				Thread.sleep(3000);
				GoToBetaAssignment.SelectNLTCourse(strCourseName);
					
			
			
				//Validate all assignment icons are displayed in Due and Past Assignment list.
			
				ViewBetaAssignment = new BetaAssignmentPage(driver);
				
				Thread.sleep(3000);
				ViewBetaAssignment.validateSortIcons_AssignDue();
				
				Thread.sleep(3000);
				ViewBetaAssignment.validateSortIcons_AssignPast();

				
				Thread.sleep(1000);
				//Verify Due Date sorting and re-sorting under Due Assignment list.

				betaAssignmentDueDate_Default = ViewBetaAssignment.getDueDateList_AssignDue();
				
				List<Date> assignmentDueDate_AssignDue = betaAssignmentDueDate_Default;
				Collections.sort(assignmentDueDate_AssignDue);
				
				List<Date> betaAssignmentDueDate_Asc = ViewBetaAssignment.getDueDateList_AssignDue();
				
				Assert.assertTrue((betaAssignmentDueDate_Asc.get(0).equals(assignmentDueDate_AssignDue.get(0))));
				Assert.assertTrue((betaAssignmentDueDate_Asc.get(1).equals(assignmentDueDate_AssignDue.get(1))));
				Assert.assertTrue((betaAssignmentDueDate_Asc.get(2).equals(assignmentDueDate_AssignDue.get(2))));
				
				Thread.sleep(1000);
				Collections.sort(assignmentDueDate_AssignDue, Collections.reverseOrder());
				
				ViewBetaAssignment.dueDateSortIcons_BottomArrow_AssignDue.click();
				List<Date> betaAssignmentDueDate_Desc = ViewBetaAssignment.getDueDateList_AssignDue();
				
				Assert.assertTrue((betaAssignmentDueDate_Desc.get(0).equals(assignmentDueDate_AssignDue.get(0))));
				Assert.assertTrue((betaAssignmentDueDate_Desc.get(1).equals(assignmentDueDate_AssignDue.get(1))));
				Assert.assertTrue((betaAssignmentDueDate_Desc.get(2).equals(assignmentDueDate_AssignDue.get(2))));

			
				//Verify Due Date sorting and re-sorting under Past Assignment list.

				Thread.sleep(1000);
				betaAssignmentPastDate_Default = ViewBetaAssignment.getDueDateList_AssignPast();
				
				List<Date> assignmentDueDateList_AssignPast = betaAssignmentPastDate_Default;
				Collections.sort(assignmentDueDateList_AssignPast);
				
				List<Date> betaAssignmentPastDate_Asc = ViewBetaAssignment.getDueDateList_AssignPast();
				
				Assert.assertTrue((betaAssignmentPastDate_Asc.get(0).equals(assignmentDueDateList_AssignPast.get(0))));
				Assert.assertTrue((betaAssignmentPastDate_Asc.get(1).equals(assignmentDueDateList_AssignPast.get(1))));
				Assert.assertTrue((betaAssignmentPastDate_Asc.get(2).equals(assignmentDueDateList_AssignPast.get(2))));
				
				Thread.sleep(1000);
				Collections.sort(assignmentDueDateList_AssignPast, Collections.reverseOrder());
				
				ViewBetaAssignment.dueDateSortIcons_BottomArrow_AssignPast.click();
				List<Date> betaAssignmentePastDate_Desc = ViewBetaAssignment.getDueDateList_AssignPast();
				
				Assert.assertTrue((betaAssignmentePastDate_Desc.get(0).equals(assignmentDueDateList_AssignPast.get(0))));
				Assert.assertTrue((betaAssignmentePastDate_Desc.get(1).equals(assignmentDueDateList_AssignPast.get(1))));
				Assert.assertTrue((betaAssignmentePastDate_Desc.get(2).equals(assignmentDueDateList_AssignPast.get(2))));

			
				//Logout of NLT application as Student.
				NLTLogin.logoutNLT();
				
			}
			

			// Closing driver and test.

			@AfterTest
			public void closeTest() throws Exception {

				PropertiesFile.tearDownTest();

			}

}


