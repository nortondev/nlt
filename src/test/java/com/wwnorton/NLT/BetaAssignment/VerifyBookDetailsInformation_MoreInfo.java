
//Verify that the "Book Details" information is displayed correctly when user clicks on "More Info" link.

package com.wwnorton.NLT.BetaAssignment;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;
import com.wwnorton.NLT.BetaAssignment.objectFactory.BetaAssignmentPage;
import com.wwnorton.NLT.BetaAssignment.objectFactory.LaunchBetaAssignment;
import com.wwnorton.NLT.BetaAssignment.objectFactory.LoginPage;
import com.wwnorton.NLT.BetaAssignment.objectFactory.ProductConfig_PCAT;
import com.wwnorton.NLT.BetaAssignment.objectFactory.eBookDetails;
import com.wwnorton.NLT.BetaAssignment.utilities.PropertiesFile;
import com.wwnorton.NLT.BetaAssignment.utilities.ReadJsonFile;
import com.wwnorton.NLT.BetaAssignment.utilities.TestListener;

import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.model.SeverityLevel;



//Call to TestNG listeners to save test logs and attachments as screen shots on Test failure.

@Listeners({ TestListener.class })


public class VerifyBookDetailsInformation_MoreInfo extends PropertiesFile {
	
	LoginPage NLTLogin;
	ProductConfig_PCAT ProductDetails;
	LaunchBetaAssignment GoToBetaAssignment;
	BetaAssignmentPage ViewBetaAssignment;
	eBookDetails MoreInfo;
	
	String userName;
	String Password;
	String LoginName;
	String strCourseName;
	String BetaViewVourseName;
	String jCourseName;
	
	String bookTitle;
	String bookSubTitle;
	String bookEdition;
	String bookAuthor;
	String bookReadingLine;
	String bookBottomDiv;
	
	String eCourseTitle;
	String eCourseSubTitle;
	String eCourseEdition;
	String eCourseAuthor;
	
	boolean MoreInfoButtonExist;
	boolean OpeneBookButtonExist;
	
	String eBookTitle;
	String eBookSubTitle;
	String eBookEdition;
	String eBookAuthor;
	String eBookReadingLine;
	String eBookBottomDiv;
	
	
	// TestNG Annotations
		@Parameters("Browser")
		@BeforeTest
		// Call to Properties file initiate Browser and set Test URL.

		public void callPropertiesFile() throws Exception {

			PropertiesFile.readPropertiesFile();
			PropertiesFile.setBrowserConfig();
			PropertiesFile.setPCATURL();

		}

		// Read login data from Json File
		ReadJsonFile readJasonObject = new ReadJsonFile();
		JsonObject jsonobject = readJasonObject.readJson();
		

		// Allure Annotations

		@Severity(SeverityLevel.NORMAL)
		@Description("Verify that the \"Book Details\" information is displayed correctly.")
		@Stories("eBook Details")
		
		
		@Test(priority = 0)
		public void TC06_VerifyBookDetailsInformation_MoreInfo() throws Exception { 
			
			//Login to PCAT Application as Admin user.
			
			NLTLogin = new LoginPage(driver);
			
			userName = jsonobject.getAsJsonObject("AdminLoginCredentials").get("userName").getAsString();
			Password = jsonobject.getAsJsonObject("AdminLoginCredentials").get("password").getAsString();
			
			NLTLogin.loginNLT(userName, Password);
			
			wait = new WebDriverWait(driver,5);
			wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
			wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));
				

			//Capture all Product Information in PCAT application.
			
			ProductDetails = new ProductConfig_PCAT(driver);
			
			Thread.sleep(2000);
			ProductDetails.selectDiscipline("52f015097e149d83af166255");
			Thread.sleep(2000);
			ProductDetails.selectProduct("58f5644c5e4ef6cd67dfc5e9");
			
			Thread.sleep(2000);
			ProductDetails.editProductInfoButton.click();
			
			ProductDetails.removeProductTitle();
			ProductDetails.editProductTitle.sendKeys("Psychological Science");
			
			ProductDetails.removeProductSubTitle();
			//ProductDetails.editProductSubTitle.sendKeys("Volume 6");
			
			ProductDetails.removeProductEdition();
			ProductDetails.editProductEdition.sendKeys("SIXTH EDITION");
			
			ProductDetails.removeProductAuthor();
			ProductDetails.editProductAuthor.sendKeys("Michael Gazzaniga");
			
			ProductDetails.removeProductReadingLine();
			//ProductDetails.editProductReadingLine.sendKeys("Reading Line");
			
			ProductDetails.removeProductDetails();
			
			ProductDetails.removeProductDLPBottomDiv();
			ProductDetails.editDLPBottomDiv.sendKeys("The ZAPS Psychology Labs were originally developed by the ZAPS Consortium.");
			
			ProductDetails.saveProductInfoButton.click();
			
			Thread.sleep(2000);
			bookTitle = ProductDetails.productTitle.getText();
			//bookSubTitle = ProductDetails.productSubTitle.getText();
			bookEdition = ProductDetails.productEdition.getText();
			bookAuthor = ProductDetails.productAuthor.getText();
			//bookReadingLine = ProductDetails.productReadingLine.getText();
			bookBottomDiv = ProductDetails.productBottomDiv.getText();
			
			
			Thread.sleep(2000);
			ProductDetails.signOutPCAT();
		
		
			//Login to NLT Application as Student and verify Student is logged in successfully.
				
			Thread.sleep(2000);
			PropertiesFile.setURL("psychsci6");
					
			NLTLogin = new LoginPage(driver);
					
			userName = jsonobject.getAsJsonObject("StudentLoginCredentials").get("student1").getAsString();
			Password = jsonobject.getAsJsonObject("StudentLoginCredentials").get("password1").getAsString();
					
			Thread.sleep(3000);
			NLTLogin.loginNLT(userName, Password);
					
			wait = new WebDriverWait(driver,5);
			wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
			wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));
					
			Thread.sleep(3000);
			LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
			NLTLogin.verifySignin(LoginName, userName);

			
			//Launch Beta Assignment View after selecting Course. 
				
			GoToBetaAssignment = new LaunchBetaAssignment(driver);
			Thread.sleep(3000);
			GoToBetaAssignment.ClickBetaAssignmentButton();
				
			strCourseName = jsonobject.getAsJsonObject("CourseInformation").get("courseName1").getAsString();
			Thread.sleep(3000);
			GoToBetaAssignment.SelectNLTCourse(strCourseName);
			
			
			//Capture Course Information on Beta Assignment Page.
						
			MoreInfo = new eBookDetails(driver);
				
			Thread.sleep(3000);
				
			eCourseTitle = MoreInfo.courseTitle.getText();
			eCourseEdition = MoreInfo.courseEdition.getText();
			eCourseAuthor = MoreInfo.courseAuthor.getText();
				
			MoreInfoButtonExist = MoreInfo.moreInfoButton.isDisplayed();
			OpeneBookButtonExist = MoreInfo.openeBookButton.isDisplayed();

		
			//Validate Book Information with PCAT Product Information.
			
			Thread.sleep(1000);
			Assert.assertEquals(eCourseTitle, bookTitle);
			Assert.assertEquals(eCourseEdition, bookEdition);
			Assert.assertEquals(eCourseAuthor, "by " + bookAuthor);
			Assert.assertEquals(MoreInfoButtonExist, true);
			Assert.assertEquals(OpeneBookButtonExist, true);
	
		
			//Capture Book Information from book Details Overlay.
						
			MoreInfo = new eBookDetails(driver);
				
			Thread.sleep(2000);
			MoreInfo.clickMoreInfoButton();
				
			Thread.sleep(2000);
			eBookTitle = MoreInfo.eBookTitle.getText();
			//eBookSubTitle = MoreInfo.eBookSubTitle.getText();
			eBookEdition = MoreInfo.eBookEdition.getText();
			eBookAuthor = MoreInfo.eBookAuthor.getText();
			//eBookReadingLine = MoreInfo.eBookReadingLine.getText();
			eBookBottomDiv = MoreInfo.eBookBottomDiv.getText();
				
			Thread.sleep(2000);
			MoreInfo.closeeBookDetailsOverlayModal.click();

		//Validate Book Information with PCAT Product Information.
				
			Thread.sleep(2000);
			Assert.assertEquals(eBookTitle, bookTitle);
			//Assert.assertEquals(eBookSubTitle, bookSubTitle);
			Assert.assertEquals(eBookEdition, bookEdition);
			Assert.assertEquals(eBookAuthor, bookAuthor);
			//Assert.assertEquals(eBookReadingLine, bookReadingLine);
			Assert.assertEquals(eBookBottomDiv, bookBottomDiv);

			
			//Logout of NLT application as Student.
			Thread.sleep(2000);
			NLTLogin.logoutNLT();
		
		
			//Login to NLT application as an Instructor.
	
			NLTLogin = new LoginPage(driver);
					
			userName = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("userName").getAsString();
			Password = jsonobject.getAsJsonObject("InstructorLoginCredentials").get("password").getAsString();
					
			NLTLogin.loginNLT(userName, Password);
					
			wait = new WebDriverWait(driver,5);
			wait.until(ExpectedConditions.invisibilityOf(NLTLogin.Overlay));
			wait.until(ExpectedConditions.visibilityOf(NLTLogin.gear_button_username));

			LoginName = NLTLogin.gear_button_username.getText().toLowerCase();
			NLTLogin.verifySignin(LoginName, userName);
	
				
			//Launch Beta Assignment View after selecting Course.
					
			GoToBetaAssignment = new LaunchBetaAssignment(driver);
			Thread.sleep(2000);
			GoToBetaAssignment.ClickBetaAssignmentButton();
					
			strCourseName = jsonobject.getAsJsonObject("CourseInformation").get("courseName1").getAsString();
			Thread.sleep(2000);
			GoToBetaAssignment.SelectNLTCourse(strCourseName);

			
			//Capture Course Information on Beta Assignment Page.
								
			MoreInfo = new eBookDetails(driver);
						
			Thread.sleep(2000);
						
			eCourseTitle = MoreInfo.courseTitle.getText();
			eCourseEdition = MoreInfo.courseEdition.getText();
			eCourseAuthor = MoreInfo.courseAuthor.getText();
						
			MoreInfoButtonExist = MoreInfo.moreInfoButton.isDisplayed();
			OpeneBookButtonExist = MoreInfo.openeBookButton.isDisplayed();
				
				
			//Validate Book Information with PCAT Product Information.
				
			Thread.sleep(1000);
			Assert.assertEquals(eCourseTitle, bookTitle);
			Assert.assertEquals(eCourseEdition, bookEdition);
			Assert.assertEquals(eCourseAuthor, "by " + bookAuthor);
			Assert.assertEquals(MoreInfoButtonExist, true);
			Assert.assertEquals(OpeneBookButtonExist, true);
								
			
			//Capture Book Information from book Details Overlay.
								
			MoreInfo = new eBookDetails(driver);
						
			Thread.sleep(2000);
			MoreInfo.clickMoreInfoButton();
				
			Thread.sleep(2000);
			eBookTitle = MoreInfo.eBookTitle.getText();
			//eBookSubTitle = MoreInfo.eBookSubTitle.getText();
			eBookEdition = MoreInfo.eBookEdition.getText();
			eBookAuthor = MoreInfo.eBookAuthor.getText();
			//eBookReadingLine = MoreInfo.eBookReadingLine.getText();
			eBookBottomDiv = MoreInfo.eBookBottomDiv.getText();
				
			Thread.sleep(2000);
			MoreInfo.closeeBookDetailsOverlayModal.click();
		
			//Validate Book Information with PCAT Product Information.
			
			Thread.sleep(2000);
			Assert.assertEquals(eBookTitle, bookTitle);
			//Assert.assertEquals(eBookSubTitle, bookSubTitle);
			Assert.assertEquals(eBookEdition, bookEdition);
			Assert.assertEquals(eBookAuthor, bookAuthor);
			//Assert.assertEquals(eBookReadingLine, bookReadingLine);
			Assert.assertEquals(eBookBottomDiv, bookBottomDiv);
			
			//Logout of NLT application as an Instructor.

			NLTLogin.logoutNLT();
		
		}
		
		
		// Closing driver and test.

		@AfterTest
		public void closeTest() throws Exception {

			PropertiesFile.tearDownTest();

		}
		
}
